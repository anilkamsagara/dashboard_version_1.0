﻿var loginApp = angular.module("loginApp", []);

loginApp.controller('LoginController', function ($scope, $http) {
    $scope.InitializeLogin = function () {
        $scope.UserName = '';
        $scope.Password = '';
        $scope.ChkRememberMe = '';
    }
    $scope.Login = function () {
        if ($scope.UserName == '' || $scope.Password == '') {
            $scope.lblMessage = 'Enter all the details'
            $("#dvMessage").show();
            return true;
        }
        else{            
            var input = $.param({ userName: $scope.UserName, password: $scope.Password, chkRememberMe: $("#ChkRememberMe").prop('checked') });
            var config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;' } }

            $http.post(loginUrl, input, config).success(function (data, status, headers, config) {
                if (data == "0") {
                    $scope.lblMessage = 'Username and Password donot match'
                    $("#dvMessage").show();
                    return true;
                }
                else if (data == "1") {
                    window.location.href = saLoginUrl;
                }
                else if (data == "2") {
                    window.location.href = shLoginUrl;
                }
                else if (data == "3") {
                    window.location.href = dhLoginUrl;
                }
                else if (data == "4") {
                    window.location.href = baLoginUrl;
                }
                //debugger;
                //$scope.lblMessage = 'Username and Password donot match'
                //$("#dvMessage").show();
                //return true;
                //var loggedIn = '@Url.Action("SALandingPage","SADetails")';
                //window.location.href = loggedIn;
            });
        }        
    }
});