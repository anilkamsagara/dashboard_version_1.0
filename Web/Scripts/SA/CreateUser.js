﻿var createUserApp = angular.module("createUserApp", []);
createUserApp.controller("CreateUserController", function ($scope, $http) {
    $scope.CreateUserInit = function () {
        $scope.txtUserName = '';
        $scope.txtPassword = '';
        $scope.txtConfirmPassword = '';
        $scope.EmployeeType = '';
        $scope.StateType = '';
        $scope.DistrictType = '';
        $scope.RTOType = '';
        $scope.PinCodeType = '';
        $scope.txtAddress = '';
        $scope.txtCity = '';
        $scope.txtAge = '';
        $scope.txtEmail = '';
        $scope.txtMobile = '';
        $scope.txtPanCard = '';
        $scope.txtAadhar = '';
        $scope.txtRegNo = '';
        $scope.txtTelephoneNo = '';
        $scope.txtFirmType = '';
        $scope.txtFirmName = '';
        $scope.txtPartnerAddr = '';
        $scope.txtPartnerAge = '';
        $scope.txtPartnerName = '';
        $scope.txtAccountnumber = '';
        $scope.BankType = '';
        $scope.txtBankAddress = '';
        $scope.txtMICRCode = '';
        $scope.txtIFSCCode = '';
    }

    $scope.UserCredentialNext = function (isSubmit) {
        if ($scope.txtUserName == "" || $scope.txtPassword == "" || $scope.txtConfirmPassword == "") {
            $scope.lblMessage = 'Enter all the details'
            $("#dvMessage").show();
            return true;
        }
        else if ($scope.txtPassword != $scope.txtConfirmPassword) {
            $scope.lblMessage = 'Password and Confirm Password do not match'
            $("#dvMessage").show();
            return true;
        }
        else {
            if (!isSubmit) {
                $("#dvMessage").hide();
                $('.nav-tabs-custom > ul > li.active').removeClass('active');
                $('.nav-tabs-custom > ul > li > a[href="#UserType"]').tab('show');
                $http({
                    method: 'GET',
                    url: '/SADetails/GetUserRoles'
                }).
                success(function (data) {
                    $scope.UserRoles = data;
                });
            }            
        }
    }

    $scope.SelectUserRole = function () {
        if ($scope.EmployeeType) {
            $http({
                method: 'POST',
                url: '/SADetails/GetStates',
                data: JSON.stringify({ countryId: 1 })
            }).
            success(function (data) {
                $scope.State = data;
            });
        }
    }

    $scope.SelectState = function (mode) {
        var id;
        if (mode == 1) {
            id = $scope.StateType;
        }
        else {
            id = $scope.updStateType;
        }
        if (id) {
                $http({
                    method: 'POST',
                    url: '/SADetails/GetDistrict',
                    data: JSON.stringify({ stateId: id })
                }).
            success(function (dist) {
                if (mode == 1) {
                    $scope.District = dist;
                }
                else {
                    $scope.updDistrict = dist;
                }
            });
        }
    }

    $scope.SelectDistrict = function (mode) {
        if ($scope.DistrictType) {
            $http({
                method: 'POST',
                url: '/SADetails/GetRtos',
                data: JSON.stringify({ districtId: $scope.DistrictType })
            })
            .success(function (rto) {
                $scope.RTO = rto;
            });
        }
        else {
            $scope.DistrictType = '';
        }
    }

    $scope.SelectRTO = function () {
        if ($scope.RTOType) {
            $http({
                method: 'POST',
                url: '/SADetails/GetPincode',
                data: JSON.stringify({ rtoId: $scope.RTOType })
            })
            .success(function (pincode) {
                $scope.PinCode = pincode;
            });
        }
        else {
            $scope.RTOType = '';
        }
    }

    $scope.SelectCountry = function () {
        if ($scope.pCountryType) {
            $http({
                method: 'POST',
                url: '/User/GetStates',
                data: JSON.stringify({ countryId: $scope.pCountryType })
            })
            .success(function (states) {
                $scope.updStates = states;
            })
        }
    }

    $scope.UserTypeNext = function (isSubmit) {
        if ($scope.EmployeeType == "") {
            $scope.lblMessage = 'Select the Employee Type';
            $("#dvMessage").show();
            return true;
        }
        else if ($scope.EmployeeType == "2"){
            if ($scope.StateType == "") {
                $scope.lblMessage = 'Select all the details';
                $("#dvMessage").show();
                return true;
            }
        }
        else if ($scope.EmployeeType == "3") {
            if ($scope.StateType == "" || $scope.DistrictType == "" || $scope.RTOType == "") {
                $scope.lblMessage = 'Select all the details';
                $("#dvMessage").show();
                return true;
            }
        }
        else {
            if ($scope.StateType == "" || $scope.DistrictType == "" || $scope.RTOType == "" || $scope.PinCodeType == "") {
                $scope.lblMessage = 'Select all the details';
                $("#dvMessage").show();
                return true;
            }
        }
        if (!isSubmit) {
            $("#dvMessage").hide();
            $('.nav-tabs-custom > ul > li.active').removeClass('active');
            $('.nav-tabs-custom > ul > li > a[href="#Personal"]').tab('show');
        }        
    }

    $scope.UserProfileNext = function (isSubmit) {
        if ($scope.txtAddress == '' || $scope.txtCity == '' || $scope.txtAge == '' || $scope.txtEmail == '' || $scope.txtMobile == '' || $scope.txtPanCard == '' || $scope.txtAadhar == '') {
            $scope.lblMessage = 'Enter all the details';
            $("#dvMessage").show();
            return true;
        }
        else {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!$.isNumeric($scope.txtAge)) {
                $scope.lblMessage = 'Enter valid Age';
                $("#dvMessage").show();
                return true;
            }
            else if (!regex.test($scope.txtEmail)) {
                $scope.lblMessage = 'Enter valid Email ID';
                $("#dvMessage").show();
                return true;
            }
            else if (!$.isNumeric($scope.txtMobile)) {
                $scope.lblMessage = 'Enter valid Mobile No';
                $("#dvMessage").show();
                return true;
            }
            else {
                if (!isSubmit) {
                    $("#dvMessage").hide();
                    $('.nav-tabs-custom > ul > li.active').removeClass('active');
                    $('.nav-tabs-custom > ul > li > a[href="#Company"]').tab('show');
                }                
            }
        }
    }

    $scope.CompanyNext = function (isSubmit) {
        if ($scope.txtRegNo == '' || $scope.txtTelephoneNo == '' || $scope.txtFirmType == '' || $scope.txtFirmName == '') {
            $scope.lblMessage = 'Enter all the details';
            $("#dvMessage").show();
            return true;
        }
        else {
            if (!$.isNumeric($scope.txtTelephoneNo)) {
                $scope.lblMessage = 'Enter valid Telephone No';
                $("#dvMessage").show();
                return true;
            }
            else {
                if (!isSubmit) {
                    $("#dvMessage").hide();
                    $('.nav-tabs-custom > ul > li.active').removeClass('active');
                    $('.nav-tabs-custom > ul > li > a[href="#Bank"]').tab('show');
                    $http({
                        method: 'GET',
                        url: '/SADetails/GetBanksInIndia'
                    }).
                    success(function (data) {
                        $scope.Banks = data;
                    });
                }                
            }
        }
    }

    $scope.AddPartner = function (isSubmit) {
        if ($scope.txtPartnerAddr == '' || $scope.txtPartnerAge == '' || $scope.txtPartnerName == '') {
            if (!isSubmit) {
                $scope.lblPartnerMessage = 'Enter all the details';
                $("#dvPartnerMessage").show();
            }
            else {
                $scope.lblMessage = 'Enter all the details';
                $("#dvMessage").show();
            }            
            return true;
        }
        else {
            if (!$.isNumeric($scope.txtPartnerAge)) {
                if (!isSubmit) {
                    $scope.lblPartnerMessage = 'Enter valid Age';
                    $("#dvPartnerMessage").show();
                }
                else {
                    $scope.lblMessage = 'Enter valid Age';
                    $("#dvMessage").show();
                }
                return true;
            }
            else {
                $("#dvPartnerMessage").hide();
                $("#myModal").modal('hide');
            }
        }
    }

    $scope.Submit = function () {
        if ($scope.txtAccountnumber == '' || $scope.BankType == '' || $scope.txtBankAddress == '' || $scope.txtMICRCode == '' || $scope.txtIFSCCode == '') {
            $scope.lblMessage = 'Enter all the details';
            $("#dvMessage").show();
            return true;
        }
        else {
            if (!$.isNumeric($scope.txtAccountnumber)) {
                $scope.lblMessage = 'Enter valid Account No';
                $("#dvMessage").show();
                return true;
            }
            else {
                //validate data again
                if ($scope.UserCredentialNext(true)) {
                    return;
                }
                else if ($scope.UserTypeNext(true)) {
                    return;
                }
                else if ($scope.UserProfileNext(true)) {
                    return;
                }
                else if ($scope.CompanyNext(true)) {
                    return;
                }
                else if ($scope.AddPartner(true)) {
                    return;
                }
                else {
                    var userDTO = {
                        "Name": $scope.txtUserName, "Password": $scope.txtPassword, "Status": 'Approved', "RoleId": $scope.EmployeeType,
                        "PinCodeId": $scope.PinCodeType, "RTOId": $scope.RTOType, "DistrictId": $scope.DistrictType, "StateId": $scope.StateType, "CountryId": 1
                    };
                    var personalDTO = {
                        "Address": $scope.txtAddress, "City": $scope.txtCity, "Age": $scope.txtAge, "EmailId": $scope.txtEmail, "MobileNumber": $scope.txtMobile, "PanCard": $scope.txtPanCard, "AadharCardNumber": $scope.txtAadhar
                    };
                    var companyDTO = {
                        "Name": $scope.txtFirmName, "FirmType": $scope.txtFirmType, "TelephoneNumber": $scope.txtTelephoneNo, "RegistrationNumber": $scope.txtRegNo
                    };
                    var partnerDTO = {
                        "Name": $scope.txtPartnerName, "Age": $scope.txtPartnerAge, "Address": $scope.txtPartnerAddr
                    };
                    var bankDTO = {
                        "Address": $scope.txtBankAddress, "MICRCode": $scope.txtMICRCode, "IFSCCode": $scope.txtIFSCCode, "BankId": $scope.BankType
                    };
                    var accountDTO = {
                        "AccountNo": $scope.txtAccountnumber, "Funds": 0
                    };
                    $http({
                        method: 'POST',
                        url: '/SADetails/SubmitUserDetails',
                        data: JSON.stringify({ userDTO: userDTO, personalDTO: personalDTO, companyDTO: companyDTO, partnerDTO: partnerDTO, bankDTO: bankDTO, accountDTO: accountDTO })
                    }).
                    success(function (data) {
                        if (data != null) {
                            switch (data) {
                                case "-1":
                                    var message = 'User Created Successfully';
                                    $scope.DisplayMessage(message, true);
                                    break;
                                case "1":
                                    var message = 'User already Exists';
                                    $scope.DisplayMessage(message, false);
                                    break;
                                case "2":
                                    var message = 'State Head already Exists';
                                    $scope.DisplayMessage(message, false);
                                    break;
                                case "3":
                                    var message = 'District Head already Exists';
                                    $scope.DisplayMessage(message, false);
                                    break;
                                case "4":
                                    var message = 'Account could not be created';
                                    DisplayMessage(message, false);
                                    break;
                                case "5":
                                    var message = 'Bank details could not be created';
                                    $scope.DisplayMessage(message, false);
                                    break;
                                case "6":
                                    var message = 'Partner details could not be created';
                                    $scope.DisplayMessage(message, false);
                                    break;
                                case "7":
                                    var message = 'Company deatils could not be created';
                                    $scope.DisplayMessage(message, false);
                                    break;
                                case "8":
                                    var message = 'Personal details could not be created';
                                    $scope.DisplayMessage(message, false);
                                    break;
                                case "9":
                                    var message = 'User details could not be created';
                                    $scope.DisplayMessage(message, false);
                                    break;
                                default:
                                    break;
                            }
                        }
                        else {
                            var message = 'Error occured';
                            $scope.DisplayMessage(message, false);
                        }
                    });
                }
            }
        }
    }

    $scope.DisplayMessage = function (message,success) {
        $scope.lblMessage = message;
        if (success) {
            $("#lblMessage").removeClass("label-danger").addClass("label-success");
        }
        else {
            $("#lblMessage").removeClass("label-success").addClass("label-danger");
        }        
        $("#dvMessage").show();
    }
});

/***************************************************************** TONNAGE CHANGE ******************************************************************************/
var tonnageApp = angular.module("tonnageApp", []);
tonnageApp.controller("TonnageController", function ($scope, $http) {
    $scope.TonnageInit = function () {
        $scope.TonnageRange = '';
        $scope.txtAmount = '';

        $http({
            method: 'GET',
            url: '/SADetails/GetTonnageDetails'
        })
        .success(function (data) {
            $scope.Tonnage = data;
        });
    }

    $scope.TonnageChange = function () {
        if ($scope.TonnageRange) {
            $http({
                method: 'POST',
                url: '/SADetails/TonnageChange',
                data: JSON.stringify({ tonnageId: $scope.TonnageRange })
            })
            .success(function (data) {
                $scope.txtAmount = data;
            });
        }
        else {
            $scope.txtAmount = '';
        }
    }

    $scope.UpdateTonnage = function () {
        if ($scope.TonnageRange == '' || $scope.txtAmount == '') {
            $scope.lblMessage = "Enter all the details";
            $("#lblMessage").removeClass("label-success").addClass("label-danger");            
            $("#dvMessage").show();
        }
        else if (!$.isNumeric($scope.txtAmount)) {
            $scope.lblMessage = "Enter valid Amount";
            $("#lblMessage").removeClass("label-success").addClass("label-danger");
            $("#dvMessage").show();
        }
        else if ($.isNumeric($scope.txtAmount) && $scope.txtAmount <= 0) {
            $scope.lblMessage = "Amount should be greater than Zero";
            $("#lblMessage").removeClass("label-success").addClass("label-danger");
            $("#dvMessage").show();
        }
        else {
            $http({
                method: 'POST',
                url: '/SADetails/UpdateTonnage',
                data: JSON.stringify({ tonnageId: $scope.TonnageRange, cost: $scope.txtAmount })
            })
            .success(function (data) {
                if (data.toLowerCase() == "true") {
                    $scope.lblMessage = "Tonnage details updated successfully";
                    $("#lblMessage").removeClass("label-danger").addClass("label-success");
                    $("#dvMessage").show();
                }
                else {
                    $scope.lblMessage = "Tonnage could not be updated";
                    $("#lblMessage").removeClass("label-success").addClass("label-danger");
                    $("#dvMessage").show();
                }
            });
        }
    }
});
/**************************************************************************************************************************************************************/