﻿var ticketApp = angular.module("ticketApp", []);
ticketApp.controller("TicketController", function ($scope, $http) {

    $scope.TicketInit = function () {
        $scope.txtRegNo = '';
        $scope.GoodsType = '';
        $scope.TonnageRange = '';
        $scope.txtMake = '';
        $scope.txtModel = '';
        $scope.txtCC = '';
        $scope.txtLength = '';
        $scope.txtWidth = '';
        $scope.txtBreadth = '';
        $scope.txtHeight = '';
        $scope.txtEngine = '';
        $scope.txtChassis = '';
        $scope.txtFCValidityFrom = '';
        $scope.txtFCValidityTo = '';
        $scope.txtAddress = '';
        $scope.txtCity = '';
        $scope.txtState = '';
        $scope.txtDistrict = '';
        $scope.txtCountry = '';


        $http({
            method: 'GET',
            url: '/BADetails/GetGoodsType'
        })
        .success(function (data) {
            $scope.Goods = data;
        });

        $http({
            method: 'GET',
            url: '/BADetails/GetTonnageDetails'
        })
        .success(function (data) {
            $scope.Tonnage = data;
        });
    }

    $scope.DateValidator = function (dateObj) {
        debugger;
        var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
        var Val_date = dateObj;
        if (Val_date.match(dateformat)) {
            var seperator1 = Val_date.split('/');
            var seperator2 = Val_date.split('-');

            if (seperator1.length > 1) {
                var splitdate = Val_date.split('/');
            }
            else if (seperator2.length > 1) {
                var splitdate = Val_date.split('-');
            }
            var dd = parseInt(splitdate[0]);
            var mm = parseInt(splitdate[1]);
            var yy = parseInt(splitdate[2]);
            var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
            if (mm == 1 || mm > 2) {
                if (dd > ListofDays[mm - 1]) {
                    //alert('Invalid date format!');
                    return false;
                }
            }
            if (mm == 2) {
                var lyear = false;
                if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
                    lyear = true;
                }
                if ((lyear == false) && (dd >= 29)) {
                    //alert('Invalid date format!');
                    return false;
                }
                if ((lyear == true) && (dd > 29)) {
                    //alert('Invalid date format!');
                    return false;
                }
            }
        }
        else {
            //alert("Invalid date format!");
            return false;
        }
    }

    $scope.VehicleNext = function () {
        debugger;
        if ($scope.txtRegNo == '' || $scope.GoodsType == '' || $scope.TonnageRange == '' || $scope.txtMake == '' || $scope.txtModel == '' || $scope.txtCC == '' ||
            $scope.txtLength == '' || $scope.txtWidth == '' || $scope.txtBreadth == '' || $scope.txtHeight == '' || $scope.txtEngine == '' || $scope.txtChassis == '' ||
            $scope.txtFCValidityFrom == '' || $scope.txtFCValidityTo == '' || $scope.txtAddress == '' || $scope.txtCity == '' || $scope.txtState == '' || $scope.txtDistrict == '' ||
            $scope.txtCountry == '' || !$("#chkNocRto").prop('checked') || !$("#chkNocPolice").prop('checked')) {
            $scope.lblMessage = 'Enter all the details'
            $("#dvMessage").show();
            return true;
        }
        else if (!$.isNumeric($scope.txtLength)) {
            $scope.lblMessage = 'Length should be numeric';
            $("#dvMessage").show();
            return true;
        }
        else if (!$.isNumeric($scope.txtWidth)) {
            $scope.lblMessage = 'Width should be numeric';
            $("#dvMessage").show();
            return true;
        }
        else if (!$.isNumeric($scope.txtBreadth)) {
            $scope.lblMessage = 'Breadth should be numeric';
            $("#dvMessage").show();
            return true;
        }
        else if (!$.isNumeric($scope.txtHeight)) {
            $scope.lblMessage = 'Height should be numeric';
            $("#dvMessage").show();
            return true;
        }        
        else if ($scope.DateValidator($("#txtFCValidityTo").val())) {
            $scope.lblMessage = 'Enter Valid FC DateTo';
            $("#dvMessage").show();
            return true;
        }
        else if ($scope.DateValidator($("#txtFCValidityFrom").val())) {
            $scope.lblMessage = 'Enter Valid FC DateFrom';
            $("#dvMessage").show();
            return true;
        }
        else {
            $("#dvMessage").hide();
            $('.nav-tabs-custom > ul > li.active').removeClass('active');
            $('.nav-tabs-custom > ul > li > a[href="#VehicleInsuranceDetails"]').tab('show');
        }
    }
});

function DateValidator(dateObj) {
    debugger;
    var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
    var Val_date = dateObj;
    if (Val_date.match(dateformat)) {
        var seperator1 = Val_date.split('/');
        var seperator2 = Val_date.split('-');

        if (seperator1.length > 1) {
            var splitdate = Val_date.split('/');
        }
        else if (seperator2.length > 1) {
            var splitdate = Val_date.split('-');
        }
        var dd = parseInt(splitdate[0]);
        var mm = parseInt(splitdate[1]);
        var yy = parseInt(splitdate[2]);
        var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        if (mm == 1 || mm > 2) {
            if (dd > ListofDays[mm - 1]) {
                //alert('Invalid date format!');
                return false;
            }
        }
        if (mm == 2) {
            var lyear = false;
            if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
                lyear = true;
            }
            if ((lyear == false) && (dd >= 29)) {
                //alert('Invalid date format!');
                return false;
            }
            if ((lyear == true) && (dd > 29)) {
                //alert('Invalid date format!');
                return false;
            }
        }
    }
    else {
        //alert("Invalid date format!");
        return false;
    }
}

function VehicleNext() {
    debugger;
    if ($("#txtRegNo").val().length == 0 || $("#GoodsType").val() == 0 || $("#TonnageRange").val() == 0 || $("#txtMake").val().length == 0 || $("#txtModel").val().length == 0 || $("#txtCC").val().length == 0 ||
        $("#txtLength").val().length == 0 || $("#txtWidth").val().length == 0 || $("#txtBreadth").val().length == 0 || $("#txtHeight").val().length == 0 || $("#txtEngine").val().length == 0 || $("#txtChassis").val().length == 0 ||
        $("#txtFCValidityFrom").val().length == 0 || $("#txtFCValidityTo").val().length == 0 || $("#txtAddress").val().length == 0 || $("#txtCity").val().length == 0 || $("#txtState").val().length == 0 || $("#txtDistrict").val().length == 0 ||
        $("#txtCountry").val().length == 0 || $("#txtPincode").val().length == 0 || !$("#chkNocRto").prop('checked') || !$("#chkNocPolice").prop('checked')) {
        $("#lblMessage").text('Enter all the details');
        $("#dvServerMessage").hide();        
        $("#dvMessage").show();
        return true;
    }
    else if ($("#txtLength").val().length > 0 && !$.isNumeric($("#txtLength").val())) {
        $("#lblMessage").text('Length should be numeric');
        $("#dvServerMessage").hide();
        $("#dvMessage").show();
        return true;
    }
    else if ($("#txtWidth").val().length > 0 && !$.isNumeric($("#txtWidth").val())) {
        $("#lblMessage").text('Width should be numeric');
        $("#dvServerMessage").hide();
        $("#dvMessage").show();
        return true;
    }
    else if ($("#txtBreadth").val().length > 0 && !$.isNumeric($("#txtBreadth").val())) {
        $("#lblMessage").text('Breadth should be numeric');
        $("#dvServerMessage").hide();
        $("#dvMessage").show();
        return true;
    }
    else if ($("#txtHeight").val().length > 0 && !$.isNumeric($("#txtHeight").val())) {
        $("#lblMessage").text('Height should be numeric');
        $("#dvServerMessage").hide();
        $("#dvMessage").show();
        return true;
    }
    else if ($("#txtPincode").val().length > 0 && !$.isNumeric($("#txtPincode").val())) {
        $("#lblMessage").text('Pincode should be numeric');
        $("#dvServerMessage").hide();
        $("#dvMessage").show();
        return true;
    }
    else if (DateValidator($("#txtFCValidityTo").val())) {
        $("#lblMessage").text('Enter Valid FC DateTo');
        $("#dvServerMessage").hide();
        $("#dvMessage").show();
        return true;
    }
    else if (DateValidator($("#txtFCValidityFrom").val())) {
        $("#lblMessage").text('Enter Valid FC DateFrom');
        $("#dvServerMessage").hide();
        $("#dvMessage").show();
        return true;
    }
    else {
        $("#dvServerMessage").hide();
        $("#dvMessage").hide();
        $('.nav-tabs-custom > ul > li.active').removeClass('active');
        $('.nav-tabs-custom > ul > li > a[href="#VehicleInsuranceDetails"]').tab('show');
    }
}

function InsuranceNext() {
    debugger;
    var InsPhoto = $("#InsurancePhoto");
    var vehiclePhoto = $("#photo");
    var rcPhoto = $("#RCBookPhoto");
    var permitPhoto = $("#PermitPhoto");

    if ($("#txtInsValidityFrom").val().length == 0 || $("#txtInsValidityTo").val().length == 0 || $("#txtPermitEndDate").val().length == 0) {
        $("#lblMessage").text('Enter all the details');
        $("#dvServerMessage").hide();
        $("#dvMessage").show();
        return true;
    }
    else if (DateValidator($("#txtInsValidityFrom").val())) {
        $("#lblMessage").text('Enter Valid Insurance DateFrom');
        $("#dvServerMessage").hide();
        $("#dvMessage").show();
        return true;
    }
    else if (DateValidator($("#txtInsValidityTo").val())) {
        $("#lblMessage").text('Enter Valid Insurance Date To');
        $("#dvServerMessage").hide();
        $("#dvMessage").show();
        return true;
    }
    if (InsPhoto) {        
        if (InsPhoto[0].files.length == 0) {
            $("#lblMessage").text('Upload Insurance Photo');
            $("#dvServerMessage").hide();
            $("#dvMessage").show();
            return true;
        }        
    }
    if (vehiclePhoto) {
        if (vehiclePhoto[0].files.length == 0) {
            $("#lblMessage").text('Upload Vehicle Photo');
            $("#dvServerMessage").hide();
            $("#dvMessage").show();
            return true;
        }
        else if (vehiclePhoto[0].files.length > 4) {
            $("#lblMessage").text('You can upload only 4 Vehicle Photos');
            $("#dvServerMessage").hide();
            $("#dvMessage").show();
            return true;
        }
    }
    if (rcPhoto) {
        if (rcPhoto[0].files.length == 0) {
            $("#lblMessage").text('Upload RC Photo');
            $("#dvServerMessage").hide();
            $("#dvMessage").show();
            return true;
        }
    }
    if (permitPhoto) {
        if (permitPhoto[0].files.length == 0) {
            $("#lblMessage").text('Upload Permit Photo');
            $("#dvServerMessage").hide();
            $("#dvMessage").show();
            return true;
        }
    }
    $("#dvServerMessage").hide();
    $("#dvMessage").hide();
    $('.nav-tabs-custom > ul > li.active').removeClass('active');
    $('.nav-tabs-custom > ul > li > a[href="#VehicleDriverDetails"]').tab('show');
}

function SubmitTicket() {
    debugger;
    var driverPhoto = $("#DriverPhoto");
    if ($("#txtDriverName").val().length == 0 || $("#txtDriverAge").val().length == 0 || $("#txtDriverAddress").val().length == 0 || $("#txtDriverCity").val().length == 0
        || $("#txtDriverDistrict").val().length == 0 || $("#txtDriverState").val().length == 0 || $("#txtDriverCountry").val().length == 0 || $("#txtDriverPanCard").val().length == 0
        || $("#txtDriverAadhar").val().length == 0 || $("#txtDriverMobNo").val().length == 0 || $("#txtDLNo").val().length == 0 || $("#txtDLValidity").val().length == 0
        || (driverPhoto && driverPhoto[0].files.length == 0) || $("#txtAmount").val().length == 0) {
        $("#lblMessage").text('Enter all the details');
        $("#dvServerMessage").hide();
        $("#dvMessage").show();
        return false;
    }
    else if ($("#txtDriverMobNo") && !$.isNumeric($("#txtDriverMobNo").val()) && $("#txtDriverMobNo").val().length != 10) {
        $("#lblMessage").text('Enter Correct Mobile No');
        $("#dvServerMessage").hide();
        $("#dvMessage").show();
        return false;
    }
    else if ($("#txtAmount") && !$.isNumeric($("#txtAmount").val())) {
        $("#lblMessage").text('Enter Correct Amount');
        $("#dvServerMessage").hide();
        $("#dvMessage").show();
        return false;
    }
    else if (DateValidator($("#txtDLValidity").val())) {
        $("#lblMessage").text('Enter Valid DL Date');
        $("#dvServerMessage").hide();
        $("#dvMessage").show();
        return false;
    }
    else if (DateValidator($("#txtPermitEndDate").val())) {
        $("#lblMessage").text('Enter Valid Permit Date');
        $("#dvServerMessage").hide();
        $("#dvMessage").show();
        return false;
    }
    else {
        return true;
    }
}