﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using DataTransferObject;
using ServiceLayer;

namespace Web.Controllers
{
    public class BADetailsController : BaseController
    {
        //
        // GET: /BADetails/

        #region Views

        public ActionResult BALandingPage()
        {
            return View();
        }

        public ActionResult BAPayDepositAmountPage()
        {
            return View();
        }

        public ActionResult BACheckCommissionAmountPage()
        {
            return View();
        }

        public ActionResult BACreateTicketPage(TicketDTO ticket)
        {
            GoodsTypeService goodsType = new GoodsTypeService();
            List<GoodsTypeDTO> lstGoods = goodsType.GetAllGoodsType();
            ViewBag.GoodsTypeDTO = lstGoods;

            TonnageService tService = new TonnageService();
            List<TonnageDTO> lstTonnage = tService.GetAllTonnageDetails();
            ViewBag.TonnageDTO = lstTonnage;
            if (ticket == null)
            {
                //ModelState.Clear();
                

                //ticket = new TicketDTO();
                //ticket.GoodsTypeDTO = new GoodsTypeDTO();
                //ticket.TonnageDTO = new TonnageDTO();
            }


            //if (ticket != null)
            //{
            //    if (ticket.TicketCreationStatus == -1)
            //    {
            //        ViewBag.Message = "Ticket Created Successfully";
            //    }
            //    else if (ticket.TicketCreationStatus == 1)
            //    {
            //        ViewBag.Message = "Driver details insertion Failed. Please Try again";
            //    }
            //    else if (ticket.TicketCreationStatus == 2)
            //    {
            //        ViewBag.Message = "Vehicle details insertion Failed. Please Try again";
            //    }
            //    else if (ticket.TicketCreationStatus == 3)
            //    {
            //        ViewBag.Message = "Registration No exists";
            //    }
            //}

            return View(ticket);
        }

        public ActionResult BAViewTicketsPage()
        {
            int userId = GetUserId();
            TicketService tService = new TicketService();
            List<TicketDTO> tickets = tService.GetTicketsByUserId(userId);
            return View(tickets);
        }

        public ActionResult BAUpdateVehicleLocationPage()
        {
            return View();
        }

        public ActionResult BAUpdateDriverDetailsPage()
        {
            return View();
        }

        public ActionResult BAEnableorDisableVehiclePage()
        {
            return View();
        }

        public ActionResult BAUpdateVehicleDetailsPage()
        {

            return View();
        }

        public ActionResult BAViewVehiclesRenewPage()
        {
            return View();
        }

        public ActionResult BAViewVehiclesListPage()
        {
            int userId = GetUserId();
            VehicleService vService = new VehicleService();
            List<VehicleDTO> vehicles = vService.GetVehiclesListForBA(userId);
            return View(vehicles);
        }

        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SubmitTicket(TicketDTO ticket, HttpPostedFileBase InsurancePhoto, IEnumerable<HttpPostedFileBase> photo, HttpPostedFileBase RCBookPhoto, HttpPostedFileBase PermitPhoto, HttpPostedFileBase fcPhoto
            , HttpPostedFileBase DriverPhoto)
        {
            int userId = GetUserId();
            TicketService tService = new TicketService();
            int result;
            if (ticket != null)
            {
                result = tService.CreateTicket(ticket, InsurancePhoto, photo, RCBookPhoto, PermitPhoto, fcPhoto, DriverPhoto, userId);
                ViewBag.Result = result;
            }
            GoodsTypeService goodsType = new GoodsTypeService();
            List<GoodsTypeDTO> lstGoods = goodsType.GetAllGoodsType();
            ViewBag.GoodsTypeDTO = lstGoods;

            TonnageService tonService = new TonnageService();
            List<TonnageDTO> lstTonnage = tonService.GetAllTonnageDetails();
            ViewBag.TonnageDTO = lstTonnage;

            return View("BACreateTicketPage");
        }  

        #endregion

        #region JsonResult
        public JsonResult GetGoodsType()
        {
            GoodsTypeService goodsType = new GoodsTypeService();
            List<GoodsTypeDTO> lstGoods = goodsType.GetAllGoodsType();
            return Json(lstGoods, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTonnageDetails()
        {
            TonnageService tService = new TonnageService();
            List<TonnageDTO> lstTonnage = tService.GetAllTonnageDetails();
            return Json(lstTonnage, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Private Methods
        private int GetUserId()
        {
            var identity = User.Identity as ClaimsIdentity;
            var userId = identity.Claims.FirstOrDefault().Value;
            return Convert.ToInt32(userId);
        }
        #endregion
    }
}
