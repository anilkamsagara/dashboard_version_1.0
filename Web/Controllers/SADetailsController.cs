﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataTransferObject;
using ServiceLayer;

namespace Web.Controllers
{

    //public class BaseController : Controller
    //{
    //    public BaseController()
    //    {
    //        ViewBag.Role = "SA";
    //    }
    //}

    public class SADetailsController : BaseController
    {
        
        //[AllowAnonymous]
        public ActionResult SALandingPage()
        {
            
            return View();
        }

        public ActionResult SAViewEmployeesPage()
        {
            UserService uService = new UserService();
            List<UserDTO> lstUsers = uService.GetAllUsers();
            return View(lstUsers);
        }

        public ActionResult SACreateUserPage()
        {
            return View();
        }

        public ActionResult SAChangeTonnageCostPage()
        {
            return View();
        }

        public int SubmitUserDetails(UserDTO userDTO,PersonalDTO personalDTO,CompanyDTO companyDTO,PartnerDTO partnerDTO,BankDTO bankDTO,AccountDTO accountDTO)
        {
            UserService uService = new UserService();
            int result = uService.CreateUser(userDTO, personalDTO, companyDTO, partnerDTO, bankDTO, accountDTO);
            return result;
        }

        public double TonnageChange(int tonnageId)
        {
            TonnageService tonnageService = new TonnageService();
            return tonnageService.TonnageChange(tonnageId);
        }

        public bool UpdateTonnage(int tonnageId, double cost)
        {
            TonnageService tonnageService = new TonnageService();
            return tonnageService.UpdateTonnage(tonnageId, cost);
        }

        #region JsonResult
        public JsonResult GetUserRoles()
        {
            RoleService roleService = new RoleService();
            List<RoleDTO> lstRoles = roleService.GetUserRoles();
            return Json(lstRoles, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStates(int countryId)
        {
            StateService stateService = new StateService();
            List<StateDTO> lstStates = stateService.GetStates(countryId);
            return Json(lstStates, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDistrict(int stateId)
        {
            DistrictService districtService = new DistrictService();
            List<DistrictDTO> lstDistrict = districtService.GetDistricts(stateId);
            return Json(lstDistrict, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRtos(int districtId)
        {
            RTOService rtoService = new RTOService();
            List<RtoDTO> lstRTO = rtoService.GetRtos(districtId);
            return Json(lstRTO, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPincode(int rtoId)
        {
            PinCodeService pcService = new PinCodeService();
            List<PinCodeDTO> lstPc = pcService.GetPinCode(rtoId);
            return Json(lstPc, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCountry()
        {
            CountryService cService = new CountryService();
            List<CountryDTO> lstCountry = cService.GetCountry();
            return Json(lstCountry, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBanksInIndia()
        {
            BankService bankService = new BankService();
            List<BanksInIndiaDTO> lstBanks = bankService.GetBanksInIndia();
            return Json(lstBanks, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTonnageDetails()
        {
            TonnageService tService = new TonnageService();
            List<TonnageDTO> lstTonnage = tService.GetAllTonnageDetails();
            return Json(lstTonnage, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
