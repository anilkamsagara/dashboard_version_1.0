﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ServiceLayer;

namespace Web.Controllers
{
    public class BaseController : Controller
    {
        public BaseController()
        {
            try 
	        {
                var userFromAuthCookie = System.Threading.Thread.CurrentPrincipal;
                if (userFromAuthCookie != null  && userFromAuthCookie.Identity.IsAuthenticated)
                {
                    System.Security.Claims.ClaimsIdentity identity =  userFromAuthCookie.Identity as System.Security.Claims.ClaimsIdentity;
                    string role = identity.RoleClaimType;

                    ViewBag.Role = role;

                    //RoleService roleService = new RoleService();
                    //var userRole = roleService.GetRoleDetails(Convert.ToInt32(role));
                    //if (userRole != null)
                    //{
                    //    ViewBag.Role = userRole.RoleName;           
                    //    ViewBag.Role = "SA";
                    //}
                }
                //ViewBag.Role = "DH";                
            }
	        catch (Exception)
	        {
		
		        throw;
	        }
        }
    }
}