﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class SHDetailsController : BaseController
    {
        //
        // GET: /SHDetails/

        public ActionResult SHLandingPage()
        {
            return View();
        }

        public ActionResult SHViewTeamPage()
        {
            return View();
        }

        public ActionResult SHViewComplaintReportPage()
        {
            return View();
        }

        public ActionResult SHViewTeamCommissionReportPage()
        {
            return View();
        }
    }
}
