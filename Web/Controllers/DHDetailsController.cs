﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using DataTransferObject;
using ServiceLayer;

namespace Web.Controllers
{
    public class DHDetailsController : BaseController
    {
        //
        // GET: /DHDetails/
        #region Public Methods
        public ActionResult DHLandingPage()
        {
            int userId = GetUserId();
            TicketService tService = new TicketService();
            List<TicketDTO> tickets = tService.GetDHTickets(userId);
            return View();
        }

        public ActionResult DHViewTeamPage()
        {
            return View();
        }

        public ActionResult DHViewRemainderListPage()
        {
            return View();
        }

        public ActionResult DHViewComplaintReportPage()
        {
            return View();
        }

        public ActionResult DHViewTeamCommissionReportPage()
        {
            return View();
        }

        public bool DHApproveReject(int ticketId, string comment, int status)
        {
            TicketService tService = new TicketService();
            return tService.DHApproveReject(ticketId, comment, status);
        }
        #endregion

        #region Private Methods
        private int GetUserId()
        {
            var identity = User.Identity as ClaimsIdentity;
            var userId = identity.Claims.FirstOrDefault().Value;
            return Convert.ToInt32(userId);
        }
        #endregion
    }
}
