﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataTransferObject;
using ServiceLayer;
using SystemFrameworks;
using System.Security.Principal;
using System.Web.Security;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace Web.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/

        public ActionResult Index()
        {
            return View();
        }

        //[HttpPost]
        //public int LoginClick(string userName, string password)
        //{
        //    UserService userService = new UserService();
        //    var userDTO = userService.GetLoginUser(userName, password);
        //    //if (userDTO.UserId > 0)
        //    //{
        //    //    return RedirectToAction("LoggedIn", "Login",new {id = userDTO.UserId});
        //    //}
        //    //return View();
        //    if (userDTO.UserId > 0)
        //    {
        //        IdentityHelper.SignIn(userDTO.Name, userDTO.RoleId.ToString(), false);
        //        return userDTO.UserId;
        //    }
        //    return 0;
        //}

        [HttpPost]
        public int LoginClick(string userName, string password, bool chkRememberMe)
        {
            UserService userService = new UserService();
            var userDTO = userService.GetLoginUser(userName, password);
            if (userDTO != null && userDTO.UserId > 0)
            {
                IdentityHelper.SignIn(userDTO.Name, userDTO.RoleId.ToString(), chkRememberMe, userDTO.UserId);
                return userDTO.RoleId;

                //if (userDTO.RoleId == Convert.ToInt32(RoleDTO.UserRoles.SuperAdmin))
                //{
                //    return RedirectToAction("SALandingPage", "SADetails");
                //}
                //else if (userDTO.RoleId == Convert.ToInt32(RoleDTO.UserRoles.StateHead))
                //{
                //    return RedirectToAction("SHLandingPage", "SHDetails");
                //}
                //else if (userDTO.RoleId == Convert.ToInt32(RoleDTO.UserRoles.DistrictHead))
                //{
                //    return RedirectToAction("DHLandingPage", "DHDetails");
                //}
            }
            else
            {
                return 0;
            }
            //return RedirectToAction("Index");
        }


        public ActionResult LogOut()
        {
            Session.Clear();
            Session.Abandon();
            IAuthenticationManager authenticationManager = System.Web.HttpContext.Current.GetOwinContext().Authentication;
            authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            HttpContext.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            HttpContext.Response.Cache.SetValidUntilExpires(false);
            HttpContext.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Response.Cache.SetNoStore();
            
            return RedirectToAction("Index", "Home");
        }
        
    }
}
