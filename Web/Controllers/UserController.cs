﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataTransferObject;
using ServiceLayer;

namespace Web.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/

        [AllowAnonymous]
        public ActionResult UserProfile()
        {

            return View();
        }

        public ActionResult CreateUser()
        {
            return View();
        }

        public JsonResult GetUserRoles()
        {
            RoleService roleService = new RoleService();
            List<RoleDTO> lstRoles= roleService.GetUserRoles();
            return Json(lstRoles, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStates(int countryId)
        {
            StateService stateService = new StateService();
            List<StateDTO> lstStates = stateService.GetStates(countryId);
            return Json(lstStates, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDistrict(int stateId)
        {
            DistrictService districtService = new DistrictService();
            List<DistrictDTO> lstDistrict = districtService.GetDistricts(stateId);
            return Json(lstDistrict, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRtos(int districtId)
        {
            RTOService rtoService = new RTOService();
            List<RtoDTO> lstRTO = rtoService.GetRtos(districtId);
            return Json(lstRTO, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPincode(int rtoId)
        {
            PinCodeService pcService = new PinCodeService();
            List<PinCodeDTO> lstPc = pcService.GetPinCode(rtoId);
            return Json(lstPc, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCountry()
        {
            CountryService cService = new CountryService();
            List<CountryDTO> lstCountry = cService.GetCountry();
            return Json(lstCountry, JsonRequestBehavior.AllowGet);
        }
    }
}
