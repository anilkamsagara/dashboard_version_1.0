﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using DataAccessLayer.Repository;
using DataTransferObject;

namespace BusinessManager
{
    public class TicketManager
    {
        #region Public Methods
        TicketRepository tRep = new TicketRepository();
        public TicketDTO CreateTicket(TicketDTO ticket, HttpPostedFileBase InsurancePhoto, IEnumerable<HttpPostedFileBase> photo, HttpPostedFileBase RCBookPhoto, HttpPostedFileBase PermitPhoto, HttpPostedFileBase fcPhoto
            , HttpPostedFileBase DriverPhoto)
        {
            for (int i = 0; i < photo.Count(); i++)
			{
                switch (i)
                {
                    case 0 :
                        ticket.VehicleDTO.VehiclePhoto1 = GetFileBytes(photo.ElementAt(i));
                        break;
                    case 1:
                        ticket.VehicleDTO.VehiclePhoto2 = GetFileBytes(photo.ElementAt(i));
                        break;
                    case 2:
                        ticket.VehicleDTO.VehiclePhoto3 = GetFileBytes(photo.ElementAt(i));
                        break;
                    case 3:
                        ticket.VehicleDTO.VehiclePhoto4 = GetFileBytes(photo.ElementAt(i));
                        break;
                    default:
                        break;
                }                
			}

            ticket.VehicleDTO.InsurancePhoto = GetFileBytes(InsurancePhoto);
            ticket.VehicleDTO.RCBookPhoto = GetFileBytes(RCBookPhoto);
            ticket.VehicleDTO.PermitPhoto = GetFileBytes(PermitPhoto);
            ticket.DriverDTO.FC_Photo =  GetFileBytes(fcPhoto);
            ticket.DriverDTO.DriverPhoto = GetFileBytes(DriverPhoto);

            ticket.VehicleDTO = FillVehicleDTO(ticket.VehicleDTO);
            ticket.DriverDTO = FillDriverDTO(ticket.DriverDTO);
            ticket = FillTicketDTO(ticket);
            return ticket;
        }

        public List<TicketDTO> GetTicketsByUserId(int userId)
        {
            try
            {
                VehicleRepository vRep = new VehicleRepository();
                GoodsTypeRepository gRep = new GoodsTypeRepository();
                TonnageRepository tonRep = new TonnageRepository();
                TicketStatusRepository tkRep = new TicketStatusRepository();

                List<TicketDTO> tickets = tRep.GetAll().Where(t => t.UserId == userId)
                    .Join(gRep.GetAll(), t => t.GoodsTypeId, g => g.GoodsTypeId, (t, g) => new { t, g })
                    .Join(tonRep.GetAll(), t => t.t.TonnageId, ton => ton.TonnageId, (t, ton) => new { t, ton })
                    .Join(tkRep.GetAll(), t => t.t.t.TicketStatusId, tk => tk.TicketStatusId, (t, tk) => new { t, tk })
                    .Join(vRep.GetAll(), t => t.t.t.t.VehicleId, l => l.VehicleId, (t, l) => new TicketDTO
                    {
                        Amount = t.t.t.t.Amount,
                        BAComments = t.t.t.t.BAComments,
                        DHComments = t.t.t.t.DHComments,
                        SHComments = t.t.t.t.SHComments,
                        SAComments = t.t.t.t.SAComments,
                        TicketStartDate = t.t.t.t.TicketStartDate,
                        TicketEndDate = t.t.t.t.TicketEndDate,
                        IsActive = t.t.t.t.IsActive,
                        TicketStatusId = t.t.t.t.TicketStatusId,
                        TonnageId = t.t.t.t.TonnageId,
                        VehicleDTO = new VehicleDTO { Registration = l.Registration },
                        TonnageDTO = new TonnageDTO { TonnageTo = t.t.ton.TonnageTo },
                        TicketStatusDTO = new TicketStatusDTO { TicketStatusDesc = t.tk.TicketStatusDesc}
                    }).ToList();

                return tickets;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<TicketDTO> GetDHTickets(int userId)
        {
            try
            {
                UserRepository uRep = new UserRepository();
                VehicleRepository vRep = new VehicleRepository();
                GoodsTypeRepository gRep = new GoodsTypeRepository();
                TonnageRepository tonRep = new TonnageRepository();
                TicketStatusRepository tkRep = new TicketStatusRepository();

                UserDTO usr = uRep.Get(userId);
                IEnumerable<UserDTO> Users = uRep.GetAllUsers().Where(u => u.RoleId == Convert.ToInt32(RoleDTO.UserRoles.BusinessAssociate) && u.StateId == usr.StateId && u.DistrictId == usr.DistrictId);
                List<TicketDTO> tickets = tRep.GetAll().Join(Users, t => t.UserId, u => u.UserId, (t, u) => new { t, u })
                    .Join(gRep.GetAll(), t => t.t.GoodsTypeId, g => g.GoodsTypeId, (t, g) => new { t, g })
                    .Join(tonRep.GetAll(), t => t.t.t.TonnageId, ton => ton.TonnageId, (t, ton) => new { t, ton })
                    .Join(tkRep.GetAll(), t => t.t.t.t.TicketStatusId, tk => tk.TicketStatusId, (t, tk) => new { t, tk })
                    .Join(vRep.GetAll(), t => t.t.t.t.t.VehicleId, v => v.VehicleId, (t, v) => new TicketDTO
                    {
                        Address = t.t.t.t.t.Address,
                        Amount = t.t.t.t.t.Amount,
                        BAComments = t.t.t.t.t.BAComments,
                        City = t.t.t.t.t.City,
                        Country = t.t.t.t.t.Country,
                        DHComments = t.t.t.t.t.DHComments,
                        District = t.t.t.t.t.District,
                        GoodsTypeDTO = new GoodsTypeDTO { GoodsTypeName = t.t.t.g.GoodsTypeName },
                        IsActive = t.t.t.t.t.IsActive,
                        PinCode = t.t.t.t.t.PinCode,
                        SAComments = t.t.t.t.t.SAComments,
                        SHComments = t.t.t.t.t.SHComments,
                        State = t.t.t.t.t.State,
                        TicketEndDate = t.t.t.t.t.TicketEndDate,
                        TicketStartDate = t.t.t.t.t.TicketStartDate,
                        TonnageDTO = new TonnageDTO { TonnageTo = t.t.ton.TonnageTo},
                        TicketStatusDTO = new TicketStatusDTO{TicketStatusDesc = t.tk.TicketStatusDesc},
                        TicketStatusId = t.t.t.t.t.TicketStatusId,
                        VehicleDTO = new VehicleDTO{Registration = v.Registration}
                    })
                    .Where(x => x.TicketStatusId == Convert.ToInt32(TicketStatusDTO.TicketStatus.BA_Approved))
                    .ToList();
                return tickets;
            }
            catch (Exception)
            {                
                throw;
            }
        }

        public bool DHApproveReject(int ticketId, string comment, int status)
        {
            try
            {
                TicketDTO ticket = tRep.GetById(ticketId);
                ticket.TicketStatusId = status;
                ticket.DHComments = comment;
                return tRep.Update(ticket);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        private TicketDTO FillTicketDTO(TicketDTO ticket)
        {
            ticket.CRDTM = DateTime.UtcNow;
            ticket.IsActive = false;
            ticket.IsDelete = false;
            ticket.TicketStatusId = Convert.ToInt32(TicketStatusDTO.TicketStatus.BA_Approved);
            ticket.UPDTM = DateTime.UtcNow;
            return ticket;
        }

        private VehicleDTO FillVehicleDTO(VehicleDTO vehicle)
        {
            vehicle.CRDTM = DateTime.UtcNow;
            vehicle.IsActive = true;
            vehicle.IsDelete = false;
            vehicle.UPDTM = DateTime.UtcNow;
            return vehicle;
        }

        private DriverDTO FillDriverDTO(DriverDTO driver)
        {
            driver.CRDTM = DateTime.UtcNow;
            driver.IsActive = true;
            driver.IsDelete = false;
            driver.UPDTM = DateTime.UtcNow;
            return driver;
        }

        private byte[] GetFileBytes(HttpPostedFileBase uploadedFile)
        {
            byte[] fileBytes = null;
            if (uploadedFile != null && uploadedFile.ContentLength > 0)
            {
                using (var reader = new System.IO.BinaryReader(uploadedFile.InputStream))
                {
                    fileBytes = reader.ReadBytes(uploadedFile.ContentLength);
                }
            }
            return fileBytes;
        }

        #endregion
    }
}
