﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Repository;
using DataTransferObject;

namespace BusinessManager
{
    public class VehicleManager
    {
        public List<VehicleDTO> GetVehiclesListForBA(int userId)
        {
            try
            {
                TicketRepository tRep = new TicketRepository();
                VehicleRepository vRep = new VehicleRepository();
                List<VehicleDTO> vehicles = null;
                IEnumerable<TicketDTO> lstTickets = tRep.GetAll().Where(t => t.UserId == userId);
                if (lstTickets != null && lstTickets.Count() > 0)
                {
                    IEnumerable<VehicleDTO> lstVehicles = vRep.GetAll();
                    if (lstVehicles != null && lstVehicles.Count() > 0) 
                    {
                        vehicles = lstTickets.Join(lstVehicles, t => t.VehicleId, l => l.VehicleId, (t, l) => new VehicleDTO
                        {
                            Registration = l.Registration,
                            Make = l.Make,
                            Model = l.Model,
                            CubicCapacity = l.CubicCapacity,
                            EngineNumber = l.EngineNumber,
                            ChasisNumber = l.ChasisNumber,
                            FCValidityTo = l.FCValidityTo,
                            InsuranceEndDate = l.InsuranceEndDate,
                            PermitEndDate = l.PermitEndDate
                        }).ToList();
                    }                    
                }
                return vehicles;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
