﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using DataTransferObject;

namespace DataAccessLayer.Repository
{
    public class VehicleLocationRepository
    {
        private List<VehicleLocationDetail> _vehicleLocationDetails = new List<VehicleLocationDetail>();
        private List<VehicleLocationDTO> _vehicleLocationDTO = new List<VehicleLocationDTO>();

        public IEnumerable<VehicleLocationDTO> GetAll()
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _vehicleLocationDetails = gatiManagementEntities.VehicleLocationDetails.ToList();
                    _vehicleLocationDTO = CustomDTOMapper.ListVehicleLocationDetailsWrapper(_vehicleLocationDetails);
                }                
                return _vehicleLocationDTO;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string Create(VehicleLocationDTO dto)
        {
            try
            {
                var vehiclelocationDetail = CustomDTOMapper.VehicleLocationDTOWrapper(dto);            
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    gatiManagementEntities.VehicleLocationDetails.Add(vehiclelocationDetail);
                    gatiManagementEntities.SaveChanges();
                }
                _vehicleLocationDetails.Add(vehiclelocationDetail);
                return "Vehicle location Details added";
            }
            catch (Exception)
            {                
                throw;
            }
        }

        public bool Update(VehicleLocationDTO dto)
        {
            try
            {
                var vehiclelocationDetail = CustomDTOMapper.VehicleLocationDTOWrapper(dto);            
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _vehicleLocationDetails = gatiManagementEntities.VehicleLocationDetails.ToList();

                    var vehicleLocationDetails = _vehicleLocationDetails.Find(x => x.VehicleLocationId == vehiclelocationDetail.VehicleLocationId);
                    if (vehicleLocationDetails == null) return true;
                    gatiManagementEntities.VehicleLocationDetails.AddOrUpdate(vehiclelocationDetail);
                    gatiManagementEntities.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public VehicleLocationDTO GetById(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _vehicleLocationDetails = gatiManagementEntities.VehicleLocationDetails.ToList();
                }
                var vehicleLocationDetail = _vehicleLocationDetails.FirstOrDefault(x => x.VehicleLocationId == id);
                var dto = CustomDTOMapper.VehicleLocationDetailsWrapper(vehicleLocationDetail);
                return dto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var vehicleLocationDetail = gatiManagementEntities.VehicleLocationDetails.FirstOrDefault(x => x.VehicleLocationId == id);
                gatiManagementEntities.VehicleLocationDetails.Remove(vehicleLocationDetail);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}
