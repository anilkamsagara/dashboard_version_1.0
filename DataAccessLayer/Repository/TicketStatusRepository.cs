﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataTransferObject;

namespace DataAccessLayer.Repository
{
    public class TicketStatusRepository
    {
        private List<TicketStatusDetail> _TicketStatusDetails = new List<TicketStatusDetail>();
        private List<TicketStatusDTO> _TicketStatusDTO = new List<TicketStatusDTO>();

        public IEnumerable<TicketStatusDTO> GetAll()
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _TicketStatusDetails = gatiManagementEntities.TicketStatusDetails.ToList();
                    _TicketStatusDTO = CustomDTOMapper.ListTicketStatusDetailMapper(_TicketStatusDetails);
                }
                return _TicketStatusDTO;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public TicketStatusDTO GetById(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _TicketStatusDetails = gatiManagementEntities.TicketStatusDetails.ToList();
                }
                var stateDetail = _TicketStatusDetails.FirstOrDefault(x => x.TicketStatusId == id);
                var dto = CustomDTOMapper.TicketStatusDetailMapper(stateDetail);
                return dto;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
