﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataTransferObject;
using System.Data.Entity.Migrations;

namespace DataAccessLayer.Repository
{
    public class VehicleRepository
    {
        private List<VehicleDTO> _vehicleDTO = new List<VehicleDTO>();
        private List<VehicleDetail> _vehicleDetails = new List<VehicleDetail>();
        public IEnumerable<VehicleDTO> GetAll()
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _vehicleDetails = gatiManagementEntities.VehicleDetails.ToList();
                    _vehicleDTO = CustomDTOMapper.ListVehicleDetailsWrapper(_vehicleDetails);
                }
                return _vehicleDTO;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Create(VehicleDTO VehicleDTO)
        {
            try
            {
                VehicleDetail VehicleDetail = CustomDTOMapper.VehicleDTOWrapper(VehicleDTO);
                int id;
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    gatiManagementEntities.VehicleDetails.Add(VehicleDetail);
                    gatiManagementEntities.SaveChanges();
                    id = VehicleDetail.VehicleId;
                }
                return id; // refactor
            }
            catch (Exception)
            {
                throw;
            }
        }
        //public VehicleDTO GetByUserId(int id)
        //{
        //    try
        //    {
        //        using (var gatiManagementEntities = new DashboardManagementEntities())
        //        {
        //            _vehicleDetails = gatiManagementEntities.VehicleDetails.ToList();
        //        }
        //        var VehicleDetails = _vehicleDetails.FirstOrDefault(x => x.UserId == id);
        //        VehicleDTO VehicleDTO = CustomDTOMapper.VehicleDetailMapper(VehicleDetails);
        //        return VehicleDTO;
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}
        public bool Update(VehicleDTO VehicleDTO)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    VehicleDetail VehicleDetail = CustomDTOMapper.VehicleDTOWrapper(VehicleDTO);
                    _vehicleDetails = gatiManagementEntities.VehicleDetails.ToList();

                    var vehicleDetails = _vehicleDetails.Find(x => x.VehicleId == VehicleDetail.VehicleId);
                    if (vehicleDetails == null) return true;
                    gatiManagementEntities.VehicleDetails.AddOrUpdate(VehicleDetail);
                    gatiManagementEntities.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public VehicleDTO GetById(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _vehicleDetails = gatiManagementEntities.VehicleDetails.ToList();
            }
            var vehicleInsuranceDetail = _vehicleDetails.FirstOrDefault(x => x.VehicleId == id);
            VehicleDTO dto = CustomDTOMapper.VehicleDetailsWrapper(vehicleInsuranceDetail);
            return dto;
        }

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var vehicleDriverDetail = gatiManagementEntities.VehicleDetails.FirstOrDefault(x => x.VehicleId == id);
                gatiManagementEntities.VehicleDetails.Remove(vehicleDriverDetail);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}
