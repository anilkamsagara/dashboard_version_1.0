﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using DataTransferObject;

namespace DataAccessLayer.Repository
{
    public class StateRepository
    {
        private List<StateDetail> _stateDetails = new List<StateDetail>();
        private List<StateDTO> _stateDTO = new List<StateDTO>();

        public IEnumerable<StateDTO> GetAll()
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _stateDetails = gatiManagementEntities.StateDetails.ToList();
                    _stateDTO = CustomDTOMapper.ListStateDetailsWrapper(_stateDetails);
                }                
                return _stateDTO;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string Create(StateDTO dto)
        {
            try
            {
                var stateDetail = CustomDTOMapper.StateDTOWrapper(dto);            
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    gatiManagementEntities.StateDetails.Add(stateDetail);
                    gatiManagementEntities.SaveChanges();
                }
                _stateDetails.Add(stateDetail);
                return "District Details added";
            }
            catch (Exception)
            {                
                throw;
            }
        }
        public StateDTO GetByUserId(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _stateDetails = gatiManagementEntities.StateDetails.ToList();
                }
                var stateDetail = _stateDetails.FirstOrDefault(x => x.StateId == id);
                var dto = CustomDTOMapper.StateDetailsWrapper(stateDetail);
                return dto;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool Update(StateDTO dto)
        {
            try
            {
                var districtDetail = CustomDTOMapper.StateDTOWrapper(dto);            
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _stateDetails = gatiManagementEntities.StateDetails.ToList();

                    var stateDetail = _stateDetails.Find(x => x.StateId == districtDetail.StateId);
                    if (stateDetail == null) return true;
                    gatiManagementEntities.StateDetails.AddOrUpdate(districtDetail);
                    gatiManagementEntities.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {                
                throw;
            }
        }

        public StateDTO GetById(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _stateDetails = gatiManagementEntities.StateDetails.ToList();
                }
                var stateDetail = _stateDetails.FirstOrDefault(x => x.StateId == id);
                var dto = CustomDTOMapper.StateDetailsWrapper(stateDetail);
                return dto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var districtDetail = gatiManagementEntities.StateDetails.FirstOrDefault(x => x.StateId == id);
                gatiManagementEntities.StateDetails.Remove(districtDetail);
                gatiManagementEntities.SaveChanges();
            }
        }

        public List<StateDTO> GetStates(int countryId)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _stateDetails = gatiManagementEntities.StateDetails.Where(s => s.CountryId == countryId).ToList();
                    _stateDTO = CustomDTOMapper.ListStateDetailsWrapper(_stateDetails);
                }
                return _stateDTO;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
