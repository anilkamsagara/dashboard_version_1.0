﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using DataTransferObject;

namespace DataAccessLayer.Repository
{
    public class BankRepository
    {
        private List<BankDetail> _bankDetails = new List<BankDetail>();
        private List<BankDTO> _bankDTO = new List<BankDTO>();
        //public IEnumerable<BankDetail> GetAll()
        public IEnumerable<BankDTO> GetAll()
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _bankDetails = gatiManagementEntities.BankDetails.ToList();
                _bankDTO = CustomDTOMapper.ListBankDetailMapper(_bankDetails);
            }
            return _bankDTO;
        }
        public int Create(BankDTO dto)
        {
            try
            {
                var bankDetail = CustomDTOMapper.BankDTOMapper(dto);
                int id;
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    gatiManagementEntities.BankDetails.Add(bankDetail);
                    gatiManagementEntities.SaveChanges();
                    id = bankDetail.BankDetailsId;
                }
                return id;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Update(BankDTO bankDTO)
        {
            try
            {
                var bankDetail = CustomDTOMapper.BankDTOMapper(bankDTO);
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _bankDetails = gatiManagementEntities.BankDetails.ToList();

                    var vehicleDetails = _bankDetails.Find(x => x.BankId == bankDetail.BankId);
                    if (vehicleDetails == null) return true;
                    gatiManagementEntities.BankDetails.AddOrUpdate(bankDetail);
                    gatiManagementEntities.SaveChanges();
                }
            return true;    
            
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public BankDTO GetById(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _bankDetails = gatiManagementEntities.BankDetails.ToList();
                }
                var bankDetail = _bankDetails.FirstOrDefault(bank => bank.BankId == id);
                var dto = CustomDTOMapper.BankDetailMapper(bankDetail);
                return dto;
            }
            catch (Exception)
            {

                throw;
            }
        }

        //public BankDetail GetByUserId(int id)
        //{
        //    using (var gatiManagementEntities = new DashboardManagementEntities())
        //    {
        //        _bankDetails = gatiManagementEntities.BankDetails.ToList();
        //    }
        //    var bankDetail = _bankDetails.FirstOrDefault(bank => bank.UserId == id);
        //    return bankDetail;
        //}

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var bankDetail = gatiManagementEntities.BankDetails.FirstOrDefault(bank => bank.BankId == id);
                gatiManagementEntities.BankDetails.Remove(bankDetail);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}
