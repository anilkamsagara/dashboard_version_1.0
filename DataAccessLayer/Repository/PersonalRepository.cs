﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using DataTransferObject;

namespace DataAccessLayer.Repository
{
    public class PersonalRepository
    {
        private List<PersonalDetail> _personalDetails = new List<PersonalDetail>();
        private List<PersonalDTO> _personalDTO = new List<PersonalDTO>();

        public IEnumerable<PersonalDTO> GetAll()
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _personalDetails = gatiManagementEntities.PersonalDetails.ToList();
                    _personalDTO = CustomDTOMapper.ListPersonalDetailsWrapper(_personalDetails);
                }                
                return _personalDTO;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Create(PersonalDTO dto)
        {
            try
            {
                var personalDetail = CustomDTOMapper.PersonalDTOWrapper(dto);
                int id;
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    gatiManagementEntities.PersonalDetails.Add(personalDetail);
                    gatiManagementEntities.SaveChanges();
                    id = personalDetail.PersonalId;
                }
                return id;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Update(PersonalDTO dto)
        {
            try
            {
                var personalDetail = CustomDTOMapper.PersonalDTOWrapper(dto);
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _personalDetails = gatiManagementEntities.PersonalDetails.ToList();
                    var personalDetails = _personalDetails.Find(x => x.PersonalId == personalDetail.PersonalId);
                    if (personalDetails == null) return true;
                    gatiManagementEntities.PersonalDetails.AddOrUpdate(personalDetail);
                    gatiManagementEntities.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public PersonalDTO GetById(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _personalDetails = gatiManagementEntities.PersonalDetails.ToList();
                }
                var user = _personalDetails.FirstOrDefault(x => x.PersonalId == id);
                var dto = CustomDTOMapper.PersonalDetailsWrapper(user);
                return dto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public PersonalDTO GetByUserId(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _personalDetails = gatiManagementEntities.PersonalDetails.ToList();
                }
                var user = _personalDetails.FirstOrDefault(x => x.UserId == id);
                var dto = CustomDTOMapper.PersonalDetailsWrapper(user);
                return dto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var user = gatiManagementEntities.PersonalDetails.FirstOrDefault(users => users.UserId == id);
                gatiManagementEntities.PersonalDetails.Remove(user);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}
