﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using DataTransferObject;

namespace DataAccessLayer.Repository
{
    public class RoleRepository
    {
        private List<RoleDetail> _roleDetails = new List<RoleDetail>();
        private List<RoleDTO> _roleDTO = new List<RoleDTO>();

        public IEnumerable<RoleDTO> GetAll()
        {
            try
            {            
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _roleDetails = gatiManagementEntities.RoleDetails.ToList();
                    _roleDTO = CustomDTOMapper.ListRoleDetailsWrapper(_roleDetails);
                }                
                return _roleDTO;
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public IEnumerable<RoleDTO> GetUserRoles()
        //{
        //    try
        //    {
        //        using (var gatiManagementEntities = new DashboardManagementEntities())
        //        {
        //            _roleDetails = gatiManagementEntities.RoleDetails.ToList();
        //        }
        //        return _roleDetails;
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        public string Create(RoleDTO dto)
        {
            try
            {
                var RoleDetail = CustomDTOMapper.RoleDTOWrapper(dto);
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    gatiManagementEntities.RoleDetails.Add(RoleDetail);
                    gatiManagementEntities.SaveChanges();
                }
                _roleDetails.Add(RoleDetail);
                return "Role Details added";
            }
            catch (Exception)
            {

                throw;
            }
        }
        public RoleDTO GetByUserId(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _roleDetails = gatiManagementEntities.RoleDetails.ToList();
                }
                var RoleDetails = _roleDetails.FirstOrDefault(x => x.RoleId == id);
                var dto = CustomDTOMapper.RoleDetailsWrapper(RoleDetails);
                return dto;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool Update(RoleDTO dto)
        {
            try
            {
                var RoleDetail = CustomDTOMapper.RoleDTOWrapper(dto);
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _roleDetails = gatiManagementEntities.RoleDetails.ToList();

                    var RoleDetails = _roleDetails.Find(x => x.RoleId == RoleDetail.RoleId);
                    if (RoleDetails == null) return true;
                    gatiManagementEntities.RoleDetails.AddOrUpdate(RoleDetail);
                    gatiManagementEntities.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public RoleDTO GetById(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _roleDetails = gatiManagementEntities.RoleDetails.ToList();
                }
                var RoleDetail = _roleDetails.FirstOrDefault(x => x.RoleId == id);
                var dto = CustomDTOMapper.RoleDetailsWrapper(RoleDetail);
                return dto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var RoleDetail = gatiManagementEntities.RoleDetails.FirstOrDefault(x => x.RoleId == id);
                gatiManagementEntities.RoleDetails.Remove(RoleDetail);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}
