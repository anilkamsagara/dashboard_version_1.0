﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using DataTransferObject;

namespace DataAccessLayer.Repository
{
    public class DocumentRepository
    {
        private List<DocumentDetail> _documentDetails = new List<DocumentDetail>();
        private List<DocumentDTO> _documentDTO = new List<DocumentDTO>();

        public IEnumerable<DocumentDTO> GetAll()
        {
            try
            {            
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _documentDetails = gatiManagementEntities.DocumentDetails.ToList();
                    _documentDTO = CustomDTOMapper.ListDocumentDetailsWrapper(_documentDetails);
                }                
                return _documentDTO;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string Create(DocumentDTO dto)
        {
            try
            {
                var documentDetail = CustomDTOMapper.DocumenttDTOWrapper(dto);
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    gatiManagementEntities.DocumentDetails.Add(documentDetail);
                    gatiManagementEntities.SaveChanges();
                }
                _documentDetails.Add(documentDetail);
                return "Document Details added";
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Update(DocumentDTO dto)
        {
            try
            {
                var documentDetail = CustomDTOMapper.DocumenttDTOWrapper(dto);
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _documentDetails = gatiManagementEntities.DocumentDetails.ToList();
                    var personalDetails = _documentDetails.Find(x => x.DocumentId == documentDetail.DocumentId);
                    if (personalDetails == null) return true;
                    gatiManagementEntities.DocumentDetails.AddOrUpdate(documentDetail);
                    gatiManagementEntities.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public DocumentDTO GetById(int id)
        {
            try
            {            
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _documentDetails = gatiManagementEntities.DocumentDetails.ToList();
                }
                var bankDetail = _documentDetails.FirstOrDefault(bank => bank.DocumentId == id);
                var dto = CustomDTOMapper.DocumenttDetailsWrapper(bankDetail);
                return dto;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public DocumentDTO GetByUserId(int id)
        {
            try
            {            
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _documentDetails = gatiManagementEntities.DocumentDetails.ToList();
                }
                var bankDetail = _documentDetails.FirstOrDefault(bank => bank.UserId == id);
                var dto = CustomDTOMapper.DocumenttDetailsWrapper(bankDetail);
                return dto;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var bankDetail = gatiManagementEntities.DocumentDetails.FirstOrDefault(bank => bank.DocumentId == id);
                gatiManagementEntities.DocumentDetails.Remove(bankDetail);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}
