﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using DataTransferObject;

namespace DataAccessLayer.Repository
{
    public class TonnageRepository
    {
        private List<TonnageDetail> _tonnageDetails = new List<TonnageDetail>();
        private List<TonnageDTO> _tonnageDTO = new List<TonnageDTO>();

        public IEnumerable<TonnageDTO> GetAll()
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _tonnageDetails = gatiManagementEntities.TonnageDetails.ToList();
                    _tonnageDTO = CustomDTOMapper.ListTonnageDetailsWrapper(_tonnageDetails);
                }                
                return _tonnageDTO;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string Create(TonnageDTO dto)
        {
            try
            {
                var tonnageDetail = CustomDTOMapper.TonnageDTOWrapper(dto);            
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    gatiManagementEntities.TonnageDetails.Add(tonnageDetail);
                    gatiManagementEntities.SaveChanges();
                }
                _tonnageDetails.Add(tonnageDetail);
                return "Tonnage Details added";
            }
            catch (Exception)
            {                
                throw;
            }
        }
        public TonnageDTO GetByUserId(int id)
        {
            try
            {                 
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _tonnageDetails = gatiManagementEntities.TonnageDetails.ToList();
                }
                var tonnageDetail = _tonnageDetails.FirstOrDefault(x => x.TonnageId == id);
                var dto = CustomDTOMapper.TonnageDetailsWrapper(tonnageDetail);
                return dto;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(TonnageDTO dto)
        {
            try
            {
                var tonnageDetail = CustomDTOMapper.TonnageDTOWrapper(dto);
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _tonnageDetails = gatiManagementEntities.TonnageDetails.ToList();

                    var tonnageDetails = _tonnageDetails.Find(x => x.TonnageId == tonnageDetail.TonnageId);
                    if (tonnageDetails == null) return false;
                    gatiManagementEntities.TonnageDetails.AddOrUpdate(tonnageDetail);
                    gatiManagementEntities.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public TonnageDTO GetById(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _tonnageDetails = gatiManagementEntities.TonnageDetails.ToList();
            }
            var tonnageDetail = _tonnageDetails.FirstOrDefault(x => x.TonnageId == id);
            TonnageDTO dto = CustomDTOMapper.TonnageDetailsWrapper(tonnageDetail);
            return dto;
        }

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var tonnageDetail = gatiManagementEntities.TonnageDetails.FirstOrDefault(x => x.TonnageId == id);
                gatiManagementEntities.TonnageDetails.Remove(tonnageDetail);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}
