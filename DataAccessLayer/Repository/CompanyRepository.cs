﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using DataTransferObject;

namespace DataAccessLayer.Repository
{
    public class CompanyRepository
    {
        private List<CompanyDetail> _companyDetails = new List<CompanyDetail>();
        private List<CompanyDTO> _companyDTO = new List<CompanyDTO>();
        
        public IEnumerable<CompanyDTO> GetAll()
        {
            try
            {            
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _companyDetails = gatiManagementEntities.CompanyDetails.ToList();
                    _companyDTO = CustomDTOMapper.ListCompanyDetailsWrapper(_companyDetails);
                }                
                return _companyDTO;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Create(CompanyDTO dto)
        {
            try
            {
                var companyDetail = CustomDTOMapper.CompanyDTOWrapper(dto);
                int id;
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    gatiManagementEntities.CompanyDetails.Add(companyDetail);
                    gatiManagementEntities.SaveChanges();
                    id = companyDetail.CompanyId;
                }
                return id;// companyDetail.CompanyId;  // Refactor
            }
            catch (Exception)
            {
                throw;
            }
        }

        public CompanyDTO GetById(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _companyDetails = gatiManagementEntities.CompanyDetails.ToList();
                }
                var companyDetail = _companyDetails.FirstOrDefault(company => company.CompanyId == id);
                var dto = CustomDTOMapper.CompanyDetailsWrapper(companyDetail);
                return dto;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public CompanyDTO GetByUserId(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _companyDetails = gatiManagementEntities.CompanyDetails.ToList();
                }
                var companyDetail = _companyDetails.FirstOrDefault(company => company.UserId == id);
                var dto = CustomDTOMapper.CompanyDetailsWrapper(companyDetail);
                return dto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var companyDetail = gatiManagementEntities.CompanyDetails.FirstOrDefault(x => x.CompanyId == id);
                gatiManagementEntities.CompanyDetails.Remove(companyDetail);
                gatiManagementEntities.SaveChanges();
            }
        }

        public bool Update(CompanyDTO dto)
        {
            var companyDetail = CustomDTOMapper.CompanyDTOWrapper(dto);
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _companyDetails = gatiManagementEntities.CompanyDetails.ToList();
                var personalDetails = _companyDetails.Find(x => x.CompanyId == companyDetail.CompanyId);
                if (personalDetails == null) return true;
                gatiManagementEntities.CompanyDetails.AddOrUpdate(companyDetail);
                gatiManagementEntities.SaveChanges();
            }
            return true;
        }
    }
}
