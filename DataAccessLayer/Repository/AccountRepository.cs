﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using DataTransferObject;

namespace DataAccessLayer.Repository
{
    public class AccountRepository
    {
        private List<AccountDTO> _accountDTO = new List<AccountDTO>();
        private List<AccountDetail> _accountDetails = new List<AccountDetail>();
        public IEnumerable<AccountDTO> GetAll()
        {
            try
            {            
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _accountDetails = gatiManagementEntities.AccountDetails.ToList();
                    _accountDTO = CustomDTOMapper.ListAccountDetailMapper(_accountDetails);
                }                
                return _accountDTO;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Create(AccountDTO accountDto)
        {
            try
            {
                AccountDetail accountDetail = CustomDTOMapper.AccountDTOMapper(accountDto);
                int id;
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    gatiManagementEntities.AccountDetails.Add(accountDetail);
                    gatiManagementEntities.SaveChanges();
                    id = accountDetail.AccountId;
                }
                return id; // refactor
            }
            catch (Exception)
            {                
                throw;
            }            
        }
        public AccountDTO GetByUserId(int id)
        {
            try
            {
                AccountDTO accountDTO = null;
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _accountDetails = gatiManagementEntities.AccountDetails.ToList();
                    var accountDetails = _accountDetails.FirstOrDefault(x => x.UserId == id);
                    accountDTO = CustomDTOMapper.AccountDetailMapper(accountDetails);
                }
                
                return accountDTO;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Update(AccountDTO accountDto)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    AccountDetail accountDetail = CustomDTOMapper.AccountDTOMapper(accountDto);
                    _accountDetails = gatiManagementEntities.AccountDetails.ToList();

                    var vehicleDetails = _accountDetails.Find(x => x.AccountId == accountDetail.AccountId);
                    if (vehicleDetails == null) return true;
                    gatiManagementEntities.AccountDetails.AddOrUpdate(accountDetail);
                    gatiManagementEntities.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public AccountDTO GetById(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _accountDetails = gatiManagementEntities.AccountDetails.ToList();
            }
            var vehicleInsuranceDetail = _accountDetails.FirstOrDefault(x => x.AccountId == id);
            AccountDTO dto = CustomDTOMapper.AccountDetailMapper(vehicleInsuranceDetail);
            return dto;
        }

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var vehicleDriverDetail = gatiManagementEntities.AccountDetails.FirstOrDefault(x => x.AccountId == id);
                gatiManagementEntities.AccountDetails.Remove(vehicleDriverDetail);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}
