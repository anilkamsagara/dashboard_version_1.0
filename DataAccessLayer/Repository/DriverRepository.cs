﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataTransferObject;
using System.Data.Entity.Migrations;

namespace DataAccessLayer.Repository
{
    public class DriverRepository
    {
        private List<DriverDTO> _DriverDTO = new List<DriverDTO>();
        private List<DriverDetail> _DriverDetails = new List<DriverDetail>();
        public IEnumerable<DriverDTO> GetAll()
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _DriverDetails = gatiManagementEntities.DriverDetails.ToList();
                    _DriverDTO = CustomDTOMapper.ListDriverDetailsWrapper(_DriverDetails);
                }
                return _DriverDTO;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Create(DriverDTO DriverDTO)
        {
            try
            {
                DriverDetail DriverDetail = CustomDTOMapper.DriverDTOWrapper(DriverDTO);
                int id;
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    gatiManagementEntities.DriverDetails.Add(DriverDetail);
                    gatiManagementEntities.SaveChanges();
                    id = DriverDetail.DriverId;
                }
                return id; // refactor
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Update(DriverDTO DriverDTO)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    DriverDetail DriverDetail = CustomDTOMapper.DriverDTOWrapper(DriverDTO);
                    _DriverDetails = gatiManagementEntities.DriverDetails.ToList();

                    var DriverDetails = _DriverDetails.Find(x => x.DriverId == DriverDetail.DriverId);
                    if (DriverDetails == null) return true;
                    gatiManagementEntities.DriverDetails.AddOrUpdate(DriverDetail);
                    gatiManagementEntities.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DriverDTO GetById(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                _DriverDetails = gatiManagementEntities.DriverDetails.ToList();
            }
            var DriverInsuranceDetail = _DriverDetails.FirstOrDefault(x => x.DriverId == id);
            DriverDTO dto = CustomDTOMapper.DriverDetailsWrapper(DriverInsuranceDetail);
            return dto;
        }

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var DriverDriverDetail = gatiManagementEntities.DriverDetails.FirstOrDefault(x => x.DriverId == id);
                gatiManagementEntities.DriverDetails.Remove(DriverDriverDetail);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}
