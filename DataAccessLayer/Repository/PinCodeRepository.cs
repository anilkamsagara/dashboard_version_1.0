﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using DataTransferObject;

namespace DataAccessLayer.Repository
{
    public class PinCodeRepository
    {
        private List<PinCodeDetail> _pinCodeDetails = new List<PinCodeDetail>();
        private List<PinCodeDTO> _pinCodeDTO = new List<PinCodeDTO>();

        public IEnumerable<PinCodeDTO> GetAll()
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _pinCodeDetails = gatiManagementEntities.PinCodeDetails.ToList();
                    _pinCodeDTO = CustomDTOMapper.ListPinCodeDetailsWrapper(_pinCodeDetails);
                }                
                return _pinCodeDTO;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public string Create(PinCodeDTO dto)
        {
            try
            {
                var pinCodeDetail = CustomDTOMapper.PinCodeDTOWrapper(dto);
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    gatiManagementEntities.PinCodeDetails.Add(pinCodeDetail);
                    gatiManagementEntities.SaveChanges();
                }
                _pinCodeDetails.Add(pinCodeDetail);
                return "RTO Details added";
            }
            catch (Exception)
            {
                throw;
            }
        }
        public PinCodeDTO GetByUserId(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _pinCodeDetails = gatiManagementEntities.PinCodeDetails.ToList();
                }
                var pinCodeDetail = _pinCodeDetails.FirstOrDefault(x => x.RTOId == id);
                var dto = CustomDTOMapper.PinCodeDetailsWrapper(pinCodeDetail);
                return dto;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool Update(PinCodeDTO dto)
        {
            try
            {
                var pinCodeDetail = CustomDTOMapper.PinCodeDTOWrapper(dto);
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _pinCodeDetails = gatiManagementEntities.PinCodeDetails.ToList();

                    var rtoDetails = _pinCodeDetails.Find(x => x.RTOId == pinCodeDetail.RTOId);
                    if (rtoDetails == null) return true;
                    gatiManagementEntities.PinCodeDetails.AddOrUpdate(pinCodeDetail);
                    gatiManagementEntities.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public PinCodeDTO GetById(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _pinCodeDetails = gatiManagementEntities.PinCodeDetails.ToList();
                }
                var pinCodeDetail = _pinCodeDetails.FirstOrDefault(x => x.PinCodeId == id);
                var dto = CustomDTOMapper.PinCodeDetailsWrapper(pinCodeDetail);
                return dto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    var rtoDetail = gatiManagementEntities.PinCodeDetails.FirstOrDefault(x => x.PinCodeId == id);
                    gatiManagementEntities.PinCodeDetails.Remove(rtoDetail);
                    gatiManagementEntities.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<PinCodeDTO> GetPinCode(int rtoId)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _pinCodeDetails = gatiManagementEntities.PinCodeDetails.Where(p => p.RTOId == rtoId).ToList();
                    _pinCodeDTO = CustomDTOMapper.ListPinCodeDetailsWrapper(_pinCodeDetails);
                }                
                return _pinCodeDTO;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
}
