﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataTransferObject;

namespace DataAccessLayer.Repository
{
    public class CountryRepository
    {
        private List<CountryDetail> _countryDetails = new List<CountryDetail>();
        private List<CountryDTO> _countryDTO = new List<CountryDTO>();

        public IEnumerable<CountryDTO> GetAll()
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _countryDetails = gatiManagementEntities.CountryDetails.ToList();
                    _countryDTO = CustomDTOMapper.ListCountryDetailsWrapper(_countryDetails);
                }
                return _countryDTO;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
