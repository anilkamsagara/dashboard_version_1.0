﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using DataTransferObject;

namespace DataAccessLayer.Repository
{
    public class PartnerRepository
    {
        private List<PartnerDetail> _partnerDetails = new List<PartnerDetail>();
        private List<PartnerDTO> _partnerDTO = new List<PartnerDTO>();
        public IEnumerable<PartnerDTO> GetAll()
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _partnerDetails = gatiManagementEntities.PartnerDetails.ToList();
                    _partnerDTO = CustomDTOMapper.ListPartnerDetailsWrapper(_partnerDetails);
                }               
                return _partnerDTO;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Create(PartnerDTO partnerDTO)
        {
            try
            {
                var partnerDetail = CustomDTOMapper.PartnerDTOWrapper(partnerDTO);
                int id;
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    gatiManagementEntities.PartnerDetails.Add(partnerDetail);
                    gatiManagementEntities.SaveChanges();
                    id = partnerDetail.PartnerId;
                }
                return id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public PartnerDTO GetById(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _partnerDetails = gatiManagementEntities.PartnerDetails.ToList();
                }
                var partnerDetail = _partnerDetails.FirstOrDefault(x => x.PartnerId == id);
                var dto = CustomDTOMapper.PartnerDetailsWrapper(partnerDetail);
                return dto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public PartnerDTO GetByUserId(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _partnerDetails = gatiManagementEntities.PartnerDetails.ToList();
                }
                var partnerDetail = _partnerDetails.FirstOrDefault(x => x.PartnerId == id);
                var dto = CustomDTOMapper.PartnerDetailsWrapper(partnerDetail);
                return dto;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void Delete(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    var partnerDetails = gatiManagementEntities.PartnerDetails.FirstOrDefault(x => x.PartnerId == id);
                    gatiManagementEntities.PartnerDetails.Remove(partnerDetails);
                    gatiManagementEntities.SaveChanges();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Update(PartnerDTO dto)
        {
            try
            {
                var ticketDetail = CustomDTOMapper.PartnerDTOWrapper(dto);
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _partnerDetails = gatiManagementEntities.PartnerDetails.ToList();
                    var partnerDetails = _partnerDetails.Find(x => x.PartnerId == ticketDetail.PartnerId);
                    if (partnerDetails == null) return true;
                    gatiManagementEntities.PartnerDetails.AddOrUpdate(ticketDetail);
                    gatiManagementEntities.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
