﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataTransferObject;

namespace DataAccessLayer.Repository
{
    public class GoodsTypeRepository
    {
        private List<GoodsTypeDetail> _goodsTypeDetails = new List<GoodsTypeDetail>();
        private List<GoodsTypeDTO> _goodsTypeDTO = new List<GoodsTypeDTO>();

        public IEnumerable<GoodsTypeDTO> GetAll()
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _goodsTypeDetails = gatiManagementEntities.GoodsTypeDetails.ToList();
                    _goodsTypeDTO = CustomDTOMapper.ListGoodsTypeDetailMapper(_goodsTypeDetails);
                }
                return _goodsTypeDTO;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public GoodsTypeDTO GetById(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _goodsTypeDetails = gatiManagementEntities.GoodsTypeDetails.ToList();
                }
                var stateDetail = _goodsTypeDetails.FirstOrDefault(x => x.GoodsTypeId == id);
                var dto = CustomDTOMapper.GoodsTypeDetailMapper(stateDetail);
                return dto;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
