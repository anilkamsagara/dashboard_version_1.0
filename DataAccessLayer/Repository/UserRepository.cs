﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using DataAccessLayer;
using DataTransferObject;

namespace DataAccessLayer.Repository
{
    public class UserRepository 
    {
        private List<UserDetail> _userList = new List<UserDetail>();
        private List<UserDTO> _userDTO = new List<UserDTO>();

        public IEnumerable<UserDTO> GetAllUsers()
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _userList = gatiManagementEntities.UserDetails.Where(u => u.IsActive && !u.IsDelete).ToList();
                    _userDTO = CustomDTOMapper.ListUserDetailMapper(_userList);
                }                
                return _userDTO;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public int CreateUser(UserDTO dto)
        {
            try
            {
                var user = CustomDTOMapper.UserDTOMapper(dto);
                int id;
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    gatiManagementEntities.UserDetails.Add(user);
                    gatiManagementEntities.SaveChanges();
                    id = user.UserId;
                }                
                return id;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public bool UpdateUser(UserDTO dto)
        {
            try
            {
                var userDetail = CustomDTOMapper.UserDTOMapper(dto);
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _userList = gatiManagementEntities.UserDetails.ToList();
                    var personalDetails = _userList.Find(x => x.Name == userDetail.Name);
                    if (personalDetails == null) return true;
                    gatiManagementEntities.UserDetails.AddOrUpdate(userDetail);
                    gatiManagementEntities.SaveChanges();
                }
                return true;
            }
            catch (System.Exception)
            {
                throw;
            }
        }
        public UserDTO Get(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _userList = gatiManagementEntities.UserDetails.ToList();
                }
                var user = _userList.FirstOrDefault(u => u.UserId == id && u.IsActive && !u.IsDelete);
                var dto = CustomDTOMapper.UserDetailMapper(user);
                return dto;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public UserDTO GetByUserName(string uName)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _userList = gatiManagementEntities.UserDetails.ToList();
                }
                var user = _userList.FirstOrDefault(u => u.Name == uName && u.IsActive && !u.IsDelete);
                var dto = CustomDTOMapper.UserDetailMapper(user);
                return dto;
            }
            catch (System.Exception)
            {
                throw;
            }
        }
        public void DeleteUser(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var user=gatiManagementEntities.UserDetails.FirstOrDefault(users => users.UserId == id);
                gatiManagementEntities.UserDetails.Remove(user);
                gatiManagementEntities.SaveChanges();
            }
        }
        public UserDTO GetLoginUser(string userName,string password)
        {
            UserDetail userDetails = null;
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                userDetails = gatiManagementEntities.UserDetails.Where(u => u.Name == userName && u.Password == password && u.IsActive && !u.IsDelete).FirstOrDefault();
                if (string.Compare(userName,userDetails.Name,false) != 0 || string.Compare(password,userDetails.Password,false) != 0)
	            {
                    userDetails = null;
	            } 
            }
            UserDTO userDTO = CustomDTOMapper.UserDetailMapper(userDetails);
            return userDTO;
        }
    }
}