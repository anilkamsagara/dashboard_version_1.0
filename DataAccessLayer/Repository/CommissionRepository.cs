﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using DataTransferObject;

namespace DataAccessLayer.Repository
{
    public class CommissionRepository
    {
        private List<CommissionDetail> _commissionDetails = new List<CommissionDetail>();
        private List<CommissionDTO> _commissionDetailsDTO = new List<CommissionDTO>();

        public IEnumerable<CommissionDTO> GetAll()
        {
            try
            {            
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _commissionDetails = gatiManagementEntities.CommissionDetails.ToList();
                    _commissionDetailsDTO = CustomDTOMapper.ListCommissionDetailsWrapper(_commissionDetails);
                }                
                return _commissionDetailsDTO;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string Create(CommissionDTO commissionDTO)
        {
            try
            {
                var commissionDetail = CustomDTOMapper.CommissionDTOWrapper(commissionDTO);
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    gatiManagementEntities.CommissionDetails.Add(commissionDetail);
                    gatiManagementEntities.SaveChanges();
                }
                _commissionDetails.Add(commissionDetail);
                return "Vehicle Driver Details added";
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Update(CommissionDTO commissionDTO)
        {
            try
            {
                var commissionDetail = CustomDTOMapper.CommissionDTOWrapper(commissionDTO);
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _commissionDetails = gatiManagementEntities.CommissionDetails.ToList();

                    var vehicleDetails = _commissionDetails.Find(x => x.CommissionId == commissionDetail.CommissionId);
                    if (vehicleDetails == null) return true;
                    gatiManagementEntities.CommissionDetails.AddOrUpdate(commissionDetail);
                    gatiManagementEntities.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public CommissionDTO GetById(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _commissionDetails = gatiManagementEntities.CommissionDetails.ToList();
                }
                var detail = _commissionDetails.FirstOrDefault(x => x.CommissionId == id);
                var dto = CustomDTOMapper.CommissionDetailsWrapper(detail);
                return dto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var vehicleDriverDetail = gatiManagementEntities.CommissionDetails.FirstOrDefault(x => x.CommissionId == id);
                gatiManagementEntities.CommissionDetails.Remove(vehicleDriverDetail);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}
