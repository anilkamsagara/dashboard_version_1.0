﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using DataTransferObject;

namespace DataAccessLayer.Repository
{
    public class RTORepository
    {
        private List<RTODetail> _rtoDetails = new List<RTODetail>();
        private List<RtoDTO> _rtoDTO = new List<RtoDTO>();

        public IEnumerable<RtoDTO> GetAll()
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _rtoDetails = gatiManagementEntities.RTODetails.ToList();
                    _rtoDTO = CustomDTOMapper.ListRTODetailsWrapper(_rtoDetails);
                }                
                return _rtoDTO;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string Create(RtoDTO dto)
        {
            try
            {
                var rtoDetail = CustomDTOMapper.RTODTOWrapper(dto);
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    gatiManagementEntities.RTODetails.Add(rtoDetail);
                    gatiManagementEntities.SaveChanges();
                }
                _rtoDetails.Add(rtoDetail);
                return "RTO Details added";
            }
            catch (Exception)
            {
                throw;
            }
        }
        public RtoDTO GetByUserId(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _rtoDetails = gatiManagementEntities.RTODetails.ToList();
                }
                var rtoDetails = _rtoDetails.FirstOrDefault(x => x.RTOId == id);
                var dto = CustomDTOMapper.RTODetailsWrapper(rtoDetails);
                return dto;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool Update(RtoDTO dto)
        {
            try
            {
                var rtoDetail = CustomDTOMapper.RTODTOWrapper(dto);
            
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _rtoDetails = gatiManagementEntities.RTODetails.ToList();

                    var rtoDetails = _rtoDetails.Find(x => x.RTOId == rtoDetail.RTOId);
                    if (rtoDetails == null) return true;
                    gatiManagementEntities.RTODetails.AddOrUpdate(rtoDetail);
                    gatiManagementEntities.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {                
                throw;
            }
        }

        public RtoDTO GetById(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _rtoDetails = gatiManagementEntities.RTODetails.ToList();
                }
                var rtoDetail = _rtoDetails.FirstOrDefault(x => x.RTOId == id);
                var dto = CustomDTOMapper.RTODetailsWrapper(rtoDetail);
                return dto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(int id)
        {
            try
            {            
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    var rtoDetail = gatiManagementEntities.RTODetails.FirstOrDefault(x => x.RTOId == id);
                    gatiManagementEntities.RTODetails.Remove(rtoDetail);
                    gatiManagementEntities.SaveChanges();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<RtoDTO> GetRtos(int districtId)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _rtoDetails = gatiManagementEntities.RTODetails.Where(d => d.DistrictId == districtId).ToList();
                    _rtoDTO = CustomDTOMapper.ListRTODetailsWrapper(_rtoDetails);
                }                
                return _rtoDTO;
            }
            catch (Exception)
            {                
                throw;
            }
        }
    }
}
