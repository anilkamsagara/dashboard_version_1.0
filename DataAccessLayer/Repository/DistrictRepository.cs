﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using DataTransferObject;

namespace DataAccessLayer.Repository
{
    public class DistrictRepository
    {
        private List<DistrictDetail> _districtDetails = new List<DistrictDetail>();
        private List<DistrictDTO> _districtDTO = new List<DistrictDTO>();

        public IEnumerable<DistrictDTO> GetAll()
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _districtDetails = gatiManagementEntities.DistrictDetails.ToList();
                    _districtDTO = CustomDTOMapper.ListDistrictDetailsWrapper(_districtDetails);
                }               
                return _districtDTO;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public string Create(DistrictDTO dto)
        {
            try
            {
                var districtDetail = CustomDTOMapper.DistrictDTOWrapper(dto);
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    gatiManagementEntities.DistrictDetails.Add(districtDetail);
                    gatiManagementEntities.SaveChanges();
                }
                _districtDetails.Add(districtDetail);
                return "District Details added";
            }
            catch (Exception)
            {

                throw;
            }
        }
        public DistrictDTO GetByUserId(int id)
        {
            try
            {            
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _districtDetails = gatiManagementEntities.DistrictDetails.ToList();
                }
                var details = _districtDetails.FirstOrDefault(x => x.DistrictId == id);
                var dto = CustomDTOMapper.DistrictDetailsWrapper(details);
                return dto;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool Update(DistrictDTO dto)
        {
            try
            {            
                var districtDetail = CustomDTOMapper.DistrictDTOWrapper(dto);
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _districtDetails = gatiManagementEntities.DistrictDetails.ToList();

                    var rtoDetails = _districtDetails.Find(x => x.DistrictId == districtDetail.DistrictId);
                    if (rtoDetails == null) return true;
                    gatiManagementEntities.DistrictDetails.AddOrUpdate(districtDetail);
                    gatiManagementEntities.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        public DistrictDTO GetById(int id)
        {
            try
            {            
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _districtDetails = gatiManagementEntities.DistrictDetails.ToList();
                }
                var districtDetail = _districtDetails.FirstOrDefault(x => x.DistrictId == id);
                var dto = CustomDTOMapper.DistrictDetailsWrapper(districtDetail);
                return dto;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void Delete(int id)
        {
            try
            {            
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    var districtDetail = gatiManagementEntities.DistrictDetails.FirstOrDefault(x => x.DistrictId == id);
                    gatiManagementEntities.DistrictDetails.Remove(districtDetail);
                    gatiManagementEntities.SaveChanges();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<DistrictDTO> GetDistricts(int stateId)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _districtDetails = gatiManagementEntities.DistrictDetails.Where(x => x.StateId == stateId).ToList();
                    _districtDTO = CustomDTOMapper.ListDistrictDetailsWrapper(_districtDetails);
                }                
                return _districtDTO;
            }
            catch (Exception)
            {                
                throw;
            }
        }
    }
}
