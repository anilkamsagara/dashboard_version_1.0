﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using DataTransferObject;

namespace DataAccessLayer.Repository
{
    public class TicketRepository
    {
        private const string TicketTemplate = "SPAP{0}";
        private List<TicketDetail> _ticketDetails = new List<TicketDetail>();
        private List<TicketDTO> _ticketDTO = new List<TicketDTO>();

        public IEnumerable<TicketDTO> GetAll()
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _ticketDetails = gatiManagementEntities.TicketDetails.ToList();
                    _ticketDTO = CustomDTOMapper.ListTicketDetailsWrapper(_ticketDetails);
                }                
                return _ticketDTO;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Create(TicketDTO dto)
        {
            try
            {
                var ticketDetail = CustomDTOMapper.TicketDTOWrapper(dto);
                int id;
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    gatiManagementEntities.TicketDetails.Add(ticketDetail);
                    gatiManagementEntities.SaveChanges();
                    id = ticketDetail.TicketId;
                }
                return id;
            }
            catch (Exception)
            {                
                throw;
            }
        }

        public TicketDTO GetById(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _ticketDetails = gatiManagementEntities.TicketDetails.ToList();
                }
                var ticketDetail = _ticketDetails.FirstOrDefault(company => company.TicketId == id);
                var dto = CustomDTOMapper.TicketDetailsWrapper(ticketDetail);
                return dto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public TicketDTO GetByUserId(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _ticketDetails = gatiManagementEntities.TicketDetails.ToList();
                }
                var ticketDetail = _ticketDetails.FirstOrDefault(t => t.UserId == id);
                var dto = CustomDTOMapper.TicketDetailsWrapper(ticketDetail);
                return dto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var companyDetail = gatiManagementEntities.TicketDetails.FirstOrDefault(x => x.TicketId == id);
                gatiManagementEntities.TicketDetails.Remove(companyDetail);
                gatiManagementEntities.SaveChanges();
            }
        }

        public bool Update(TicketDTO dto)
        {
            try
            {
                var ticketDetail = CustomDTOMapper.TicketDTOWrapper(dto);            
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _ticketDetails = gatiManagementEntities.TicketDetails.ToList();
                    var ticketDetails = _ticketDetails.Find(x => x.TicketId == ticketDetail.TicketId);
                    if (ticketDetails == null) return true;
                    gatiManagementEntities.TicketDetails.AddOrUpdate(ticketDetail);
                    gatiManagementEntities.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
}
