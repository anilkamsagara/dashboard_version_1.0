﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using DataTransferObject;

namespace DataAccessLayer.Repository
{
    public class VehicleOwnerRepository
    {
        private List<VehicleOwnerDetail> _vehicleOwnerDetails = new List<VehicleOwnerDetail>();
        private List<VehicleOwnerDTO> _vehicleOwnerDTO = new List<VehicleOwnerDTO>();

        public IEnumerable<VehicleOwnerDTO> GetAll()
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _vehicleOwnerDetails = gatiManagementEntities.VehicleOwnerDetails.ToList();
                    _vehicleOwnerDTO = CustomDTOMapper.ListVehicleOwnerDetailsWrapper(_vehicleOwnerDetails);
                }                
                return _vehicleOwnerDTO;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public string Create(VehicleOwnerDTO dto)
        {
            try
            {
                var vehicleOwnerDetail = CustomDTOMapper.VehicleOwnerDTOWrapper(dto);            
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    gatiManagementEntities.VehicleOwnerDetails.Add(vehicleOwnerDetail);
                    gatiManagementEntities.SaveChanges();
                }
                _vehicleOwnerDetails.Add(vehicleOwnerDetail);
                return "Vehicle location Details added";
            }
            catch (Exception)
            {                
                throw;
            }
        }

        public bool Update(VehicleOwnerDTO dto)
        {
            try
            {
                var vehicleOwnerDetail = CustomDTOMapper.VehicleOwnerDTOWrapper(dto);            
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _vehicleOwnerDetails = gatiManagementEntities.VehicleOwnerDetails.ToList();

                    var vehicleLocationDetails = _vehicleOwnerDetails.Find(x => x.VehicleOwnerId == vehicleOwnerDetail.VehicleOwnerId);
                    if (vehicleLocationDetails == null) return true;
                    gatiManagementEntities.VehicleOwnerDetails.AddOrUpdate(vehicleOwnerDetail);
                    gatiManagementEntities.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {                
                throw;
            }
        }

        public VehicleOwnerDTO GetById(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _vehicleOwnerDetails = gatiManagementEntities.VehicleOwnerDetails.ToList();
                }
                var vehicleLocationDetail = _vehicleOwnerDetails.FirstOrDefault(x => x.VehicleOwnerId == id);
                var dto = CustomDTOMapper.VehicleOwnerDetailsWrapper(vehicleLocationDetail);
                return dto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var vehicleLocationDetail = gatiManagementEntities.VehicleLocationDetails.FirstOrDefault(x => x.VehicleLocationId == id);
                gatiManagementEntities.VehicleLocationDetails.Remove(vehicleLocationDetail);
                gatiManagementEntities.SaveChanges();
            }
        }
    }
}
