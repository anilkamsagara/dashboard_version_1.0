﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;
using DataTransferObject;

namespace DataAccessLayer.Repository
{
    public class BanksInIndiaRepository
    {
        private List<BanksInIndia> _banksInIndias = new List<BanksInIndia>();
        private List<BanksInIndiaDTO> _banksInIndiasDTO = new List<BanksInIndiaDTO>();
        
        public IEnumerable<BanksInIndiaDTO> GetAll()
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _banksInIndias = gatiManagementEntities.BanksInIndias.ToList();
                    _banksInIndiasDTO = CustomDTOMapper.ListBanksInIndiaWrapper(_banksInIndias);
                }                
                return _banksInIndiasDTO;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string Create(BanksInIndiaDTO banksInIndiaDTO)
        {
            try
            {
                var banksInIndia = CustomDTOMapper.BanksInIndiaDTOWrapper(banksInIndiaDTO);
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    gatiManagementEntities.BanksInIndias.Add(banksInIndia);
                    gatiManagementEntities.SaveChanges();
                }
                return banksInIndia.BankId.ToString();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public BanksInIndiaDTO GetById(int id)
        {
            try
            {
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _banksInIndias = gatiManagementEntities.BanksInIndias.ToList();
                }
                var banksInIndia = _banksInIndias.FirstOrDefault(x => x.BankId == id);
                var dto = CustomDTOMapper.BanksInIndiaWrapper(banksInIndia);
                return dto;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void Delete(int id)
        {
            using (var gatiManagementEntities = new DashboardManagementEntities())
            {
                var companyDetail = gatiManagementEntities.BanksInIndias.FirstOrDefault(x => x.BankId == id);
                gatiManagementEntities.BanksInIndias.Remove(companyDetail);
                gatiManagementEntities.SaveChanges();
            }
        }

        public bool Update(BanksInIndiaDTO dto)
        {
            try
            {
                var banksInIndia = CustomDTOMapper.BanksInIndiaDTOWrapper(dto);
                using (var gatiManagementEntities = new DashboardManagementEntities())
                {
                    _banksInIndias = gatiManagementEntities.BanksInIndias.ToList();
                    var ticketDetails = _banksInIndias.Find(x => x.BankId == banksInIndia.BankId);
                    if (ticketDetails == null) return true;
                    gatiManagementEntities.BanksInIndias.AddOrUpdate(banksInIndia);
                    gatiManagementEntities.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
