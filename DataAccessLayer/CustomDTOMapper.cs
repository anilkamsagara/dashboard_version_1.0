﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataTransferObject;

namespace DataAccessLayer
{
    public class CustomDTOMapper
    {
        #region AccountDTO
        public static AccountDTO AccountDetailMapper(AccountDetail accountDetail)
        {
            AccountDTO accountDTO = null;
            if (accountDetail != null)
            {
                accountDTO = new AccountDTO();
                accountDTO.AccountId = accountDetail.AccountId;
                accountDTO.BankDetailsId = accountDetail.BankDetailsId;
                if (accountDetail.BankDetail != null)
                {
                    accountDTO.BankDTO = BankDetailMapper(accountDetail.BankDetail);
                }
                accountDTO.CRDTM = accountDetail.CRDTM;
                accountDTO.Funds = accountDetail.Funds;
                accountDTO.IsActive = accountDetail.IsActive;
                accountDTO.IsDelete = accountDetail.IsDelete;
                accountDTO.AccountNo = accountDetail.AccountNo;
                accountDTO.UserId = accountDetail.UserId;
                accountDTO.UPDTM = accountDetail.UPDTM;
                if (accountDetail.UserDetail != null)
                {
                    accountDTO.UserDTO = UserDetailMapper(accountDetail.UserDetail);
                }
            }            
            return accountDTO;
        }

        public static AccountDetail AccountDTOMapper(AccountDTO dto)
        {
            AccountDetail detail = null;
            if (dto != null)
            {
                detail = new AccountDetail();
                detail.AccountId = dto.AccountId;
                detail.BankDetailsId = dto.BankDetailsId;
                if (dto.BankDTO != null)
                {
                    detail.BankDetail = BankDTOMapper(dto.BankDTO);
                }
                detail.CRDTM = dto.CRDTM;
                detail.Funds = dto.Funds;
                detail.IsActive = dto.IsActive;
                detail.IsDelete = dto.IsDelete;
                detail.AccountNo = dto.AccountNo;
                detail.UserId = dto.UserId;
                detail.UPDTM = dto.UPDTM;
                if (dto.UserDTO != null)
                {
                    detail.UserDetail = UserDTOMapper(dto.UserDTO);
                }
            }            
            return detail;
        }

        public static List<AccountDTO> ListAccountDetailMapper(List<AccountDetail> lstDetails)
        {
            List<AccountDTO> lstDTO = new List<AccountDTO>();
            foreach (AccountDetail item in lstDetails)
            {
                AccountDTO DTO = AccountDetailMapper(item);
                lstDTO.Add(DTO);
            }
            return lstDTO;
        }

        #endregion

        #region BankDTO
        public static BankDTO BankDetailMapper(BankDetail detail)
        {
            BankDTO dto = null;
            if (detail != null)
            {
                dto = new BankDTO();
                dto.Address = detail.Address;
                dto.BankDetailsId = detail.BankDetailsId;
                dto.BankId = detail.BankId;
                if (detail.BanksInIndia != null)
                {
                    dto.BanksInIndia = BanksInIndiaWrapper(detail.BanksInIndia);
                }
                dto.CRDTM = detail.CRDTM;
                dto.IFSCCode = detail.IFSCCode;
                dto.IsActive = detail.IsActive;
                dto.IsDelete = detail.IsDelete;
                dto.MICRCode = detail.MICRCode;
                dto.UPDTM = detail.UPDTM;
            }            
            return dto;
        }

        public static BankDetail BankDTOMapper(BankDTO dto)
        {
            BankDetail detail = null;
            if (true)
            {
                detail = new BankDetail();
                detail.Address = dto.Address;
                detail.BankDetailsId = dto.BankDetailsId;
                detail.BankId = dto.BankId;
                if (dto.BanksInIndia != null)
                {
                    detail.BanksInIndia = BanksInIndiaDTOWrapper(dto.BanksInIndia);
                }
                detail.CRDTM = dto.CRDTM;
                detail.IFSCCode = dto.IFSCCode;
                detail.IsActive = dto.IsActive;
                detail.IsDelete = dto.IsDelete;
                detail.MICRCode = dto.MICRCode;
                detail.UPDTM = dto.UPDTM;
            }
            return detail;
        }

        public static List<BankDTO> ListBankDetailMapper(List<BankDetail> lstDetails)
        {
            List<BankDTO> lstDTO = new List<BankDTO>();
            foreach (BankDetail item in lstDetails)
            {
                BankDTO dto = BankDetailMapper(item);
                lstDTO.Add(dto);
            }
            return lstDTO;
        }
        #endregion

        #region BanksInIndia
        public static BanksInIndiaDTO BanksInIndiaWrapper(BanksInIndia details)
        {
            BanksInIndiaDTO dto = null;
            if (details != null)
            {
                dto = new BanksInIndiaDTO();
                dto.BankId = details.BankId;
                dto.BankName = details.BankName;
            }            
            return dto;
        }

        public static BanksInIndia BanksInIndiaDTOWrapper(BanksInIndiaDTO dto)
        {
            BanksInIndia detail = null;
            if (dto != null)
            {
                detail = new BanksInIndia();
                detail.BankId = dto.BankId;
                detail.BankName = dto.BankName;
            }
            return detail;
        }

        //public static BanksInIndiaDTO BanksInIndiaWrapper(BanksInIndiaDTO dto)
        //{
        //    BanksInIndiaDTO detail = null;
        //    if (dto != null)
        //    {
        //        detail = new BanksInIndiaDTO();
        //        detail.BankId = dto.BankId;
        //        detail.BankName = dto.BankName;
        //    }           
        //    return detail;
        //}

        public static List<BanksInIndiaDTO> ListBanksInIndiaWrapper(List<BanksInIndia> details)
        {
            List<BanksInIndiaDTO> lstDTO = null;
            if (details != null)
            {
                lstDTO = new List<BanksInIndiaDTO>();
                foreach (BanksInIndia item in details)
                {                    
                    BanksInIndiaDTO dto = BanksInIndiaWrapper(item);
                    lstDTO.Add(dto);
                }                
            }
            return lstDTO;    
        }
        #endregion

        #region CommissionDTO
        public static CommissionDTO CommissionDetailsWrapper(CommissionDetail detail)
        {
            CommissionDTO dto = null;
            if (detail != null)
            {
                dto = new CommissionDTO();
                dto.CommissionBA = detail.CommissionBA;
                dto.CommissionDH = detail.CommissionDH;
                dto.CommissionId = detail.CommissionId;
                dto.CommissionSH = detail.CommissionSH;
                dto.CRDTM = detail.CRDTM;
                dto.IsActive = detail.IsActive;
                dto.IsDelete = detail.IsDelete;
                dto.RenewalCost = detail.RenewalCost;
                dto.UPDTM = detail.UPDTM;
                if (detail.UserDetail != null)
                {
                    dto.UserDTO = UserDetailMapper(detail.UserDetail);
                }
                dto.UserId = detail.UserId;
            }            
            return dto;
        }

        public static CommissionDetail CommissionDTOWrapper(CommissionDTO dto)
        {
            CommissionDetail detail = null;
            if (dto != null)
            {
                detail = new CommissionDetail();
                detail.CommissionBA = dto.CommissionBA;
                detail.CommissionDH = dto.CommissionDH;
                detail.CommissionId = dto.CommissionId;
                detail.CommissionSH = dto.CommissionSH;
                detail.CRDTM = dto.CRDTM;
                detail.IsActive = dto.IsActive;
                detail.IsDelete = dto.IsDelete;
                detail.RenewalCost = dto.RenewalCost;
                detail.UPDTM = dto.UPDTM;
                if (dto.UserDTO != null)
                {
                    detail.UserDetail = UserDTOMapper(dto.UserDTO);
                }
                detail.UserId = dto.UserId;
            }            
            return detail;
        }

        public static List<CommissionDTO> ListCommissionDetailsWrapper(List<CommissionDetail> lstDetail)
        {
            List<CommissionDTO> lstDto = null;
            foreach (CommissionDetail item in lstDetail)
            {
                lstDto = new List<CommissionDTO>();
                CommissionDTO dto = CommissionDetailsWrapper(item);
                lstDto.Add(dto);
            }
            return lstDto;
        }
        #endregion

        #region CompanyDTO
        public static CompanyDTO CompanyDetailsWrapper(CompanyDetail detail)
        {
            CompanyDTO dto = null;
            if (detail != null)
            {
                dto = new CompanyDTO();
                dto.CompanyId = detail.CompanyId;
                dto.CRDTM = detail.CRDTM;
                dto.FirmType = detail.FirmType;
                dto.IsActive = detail.IsActive;
                dto.IsDelete = detail.IsDelete;
                dto.Name = detail.Name;
                dto.RegistrationNumber = detail.RegistrationNumber;
                dto.TelephoneNumber = detail.TelephoneNumber;
                dto.UPDTM = detail.UPDTM;
                if (detail.UserDetail!= null)
	            {
                    dto.UserDTO = UserDetailMapper(detail.UserDetail);
	            }
                dto.UserId = detail.UserId;
            }
            return dto;
        }

        public static CompanyDetail CompanyDTOWrapper(CompanyDTO dto)
        {
            CompanyDetail detail = null;
            if (dto != null)
            {
                detail = new CompanyDetail();
                detail.CompanyId = dto.CompanyId;
                detail.CRDTM = dto.CRDTM;
                detail.FirmType = dto.FirmType;
                detail.IsActive = dto.IsActive;
                detail.IsDelete = dto.IsDelete;
                detail.Name = dto.Name;
                detail.RegistrationNumber = dto.RegistrationNumber;
                detail.TelephoneNumber = dto.TelephoneNumber;
                detail.UPDTM = dto.UPDTM;
                if (dto.UserDTO != null)
                {
                    detail.UserDetail = UserDTOMapper(dto.UserDTO);
                }
                detail.UserId = dto.UserId;
            }
            return detail;
        }

        public static List<CompanyDTO> ListCompanyDetailsWrapper(List<CompanyDetail> lstDetails)
        {
            List<CompanyDTO> lstDTO = null;
            if (lstDetails != null)
            {
                lstDTO = new List<CompanyDTO>();
                foreach (CompanyDetail item in lstDetails)
                {
                    CompanyDTO dto = CompanyDetailsWrapper(item);
                    lstDTO.Add(dto);
                }
            }
            return lstDTO;
        }
        #endregion

        #region CountryDTO
        public static CountryDTO CountryDetailsWrapper(CountryDetail detail)
        {
            CountryDTO dto = null;
            if (detail != null)
            {
                dto = new CountryDTO();
                dto.CountryId = detail.CountryId;
                dto.CountryName = detail.CountryName;
                dto.CRDTM = detail.CRDTM;
                dto.IsActive = detail.IsActive;
                dto.IsDelete = detail.IsDelete;
                dto.UPDTM = detail.UPDTM;
            }
            return dto;
        }

        public static CountryDetail CountryDTOWrapper(CountryDTO dto)
        {
            CountryDetail detail = null;
            if (dto != null)
            {
                detail = new CountryDetail();
                detail.CountryId = dto.CountryId;
                detail.CountryName = dto.CountryName;
                detail.CRDTM = dto.CRDTM;
                detail.IsActive = dto.IsActive;
                detail.IsDelete = dto.IsDelete;
                detail.UPDTM = dto.UPDTM;
            }
            return detail;
        }

        public static List<CountryDTO> ListCountryDetailsWrapper(List<CountryDetail> lstdetail)
        {
            List<CountryDTO> lstDTO = null;
            if (lstdetail != null)
            {
                lstDTO = new List<CountryDTO>();
                foreach (var item in lstdetail)
	            {
                    CountryDTO dto = CountryDetailsWrapper(item);
                    lstDTO.Add(dto);
	            }
            }
            return lstDTO;
        }
        #endregion

        #region DistrictDTO
        public static DistrictDTO DistrictDetailsWrapper(DistrictDetail detail)
        {
            DistrictDTO dto = null;
            if (detail != null)
            {
                dto = new DistrictDTO();
                dto.CRDTM = detail.CRDTM;
                dto.DistrictId = detail.DistrictId;
                dto.DistrictName = detail.DistrictName;
                dto.IsActive = detail.IsActive;
                dto.IsDelete = detail.IsDelete;
                dto.StateId = detail.StateId;
                dto.UPDTM = detail.UPDTM;
            }
            return dto;
        }

        public static DistrictDetail DistrictDTOWrapper(DistrictDTO dto)
        {
            DistrictDetail detail = null;
            if (dto != null)
            {
                detail = new DistrictDetail();
                detail.CRDTM = dto.CRDTM;
                detail.DistrictId = dto.DistrictId;
                detail.DistrictName = dto.DistrictName;
                detail.IsActive = dto.IsActive;
                detail.IsDelete = dto.IsDelete;
                detail.StateId = dto.StateId;
                detail.UPDTM = dto.UPDTM;
            }
            return detail;
        }

        public static List<DistrictDTO> ListDistrictDetailsWrapper(List<DistrictDetail> lstdetail)
        {
            List<DistrictDTO> lstDTO = null;
            if (lstdetail != null)
            {
                lstDTO = new List<DistrictDTO>();
                foreach (var item in lstdetail)
                {
                    DistrictDTO dto = DistrictDetailsWrapper(item);
                    lstDTO.Add(dto);
                }
            }
            return lstDTO;
        }
        #endregion

        #region DocumentDTO
        public static DocumentDTO DocumenttDetailsWrapper(DocumentDetail detail)
        {
            DocumentDTO dto = null;
            if (detail != null)
            {
                dto = new DocumentDTO();
                dto.AadharCardProof = detail.AadharCardProof;
                dto.CompanyAddressProof = detail.CompanyAddressProof;
                dto.CompanyRegistration = detail.CompanyRegistration;
                dto.CRDTM = detail.CRDTM;
                dto.DocumentId = detail.DocumentId;
                dto.DocumentName = detail.DocumentName;                
                dto.IsActive = detail.IsActive;
                dto.IsDelete = detail.IsDelete;
                dto.MobileBill = detail.MobileBill;
                dto.OfficeRentalDocument = detail.OfficeRentalDocument;
                dto.PanCardProof = detail.PanCardProof;
                dto.PartnerAddressProof = detail.PartnerAddressProof;                
                dto.UPDTM = detail.UPDTM;
                if (detail.UserDetail != null)
                {
                    dto.UserDTO = UserDetailMapper(detail.UserDetail);
                }
                dto.UserId = detail.UserId;
            }
            return dto;
        }

        public static DocumentDetail DocumenttDTOWrapper(DocumentDTO dto)
        {
            DocumentDetail detail = null;
            if (dto != null)
            {
                detail = new DocumentDetail();
                detail.AadharCardProof = dto.AadharCardProof;
                detail.CompanyAddressProof = dto.CompanyAddressProof;
                detail.CompanyRegistration = dto.CompanyRegistration;
                detail.CRDTM = dto.CRDTM;
                detail.DocumentId = dto.DocumentId;
                detail.DocumentName = dto.DocumentName;
                detail.IsActive = dto.IsActive;
                detail.IsDelete = dto.IsDelete;
                detail.MobileBill = dto.MobileBill;
                detail.OfficeRentalDocument = dto.OfficeRentalDocument;
                detail.PanCardProof = dto.PanCardProof;
                detail.PartnerAddressProof = dto.PartnerAddressProof;
                detail.UPDTM = dto.UPDTM;
                if (dto.UserDTO != null)
                {
                    detail.UserDetail = UserDTOMapper(dto.UserDTO);
                }
                detail.UserId = dto.UserId;
            }
            return detail;
        }

        public static List<DocumentDTO> ListDocumentDetailsWrapper(List<DocumentDetail> lstdetail)
        {
            List<DocumentDTO> lstDTO = null;
            if (lstdetail != null)
            {
                lstDTO = new List<DocumentDTO>();
                foreach (var item in lstdetail)
                {
                    DocumentDTO dto = DocumenttDetailsWrapper(item);
                    lstDTO.Add(dto);
                }
            }
            return lstDTO;
        }
        #endregion

        #region DriverDTO
        public static DriverDTO DriverDetailsWrapper(DriverDetail detail)
        {
            DriverDTO dto = null;
            if (detail != null)
            {
                dto = new DriverDTO();
                dto.AadharCard = detail.AadharCard;
                dto.Address = detail.Address;
                dto.Age = detail.Age;
                dto.City = detail.City;
                //if (detail.CountryDetail != null)
                //{
                //    dto.CountryDTO = CountryDetailsWrapper(detail.CountryDetail);
                //}
                dto.Country = detail.Country;
                dto.District = detail.District;
                dto.DLNumber = detail.DLNumber;
                dto.DLValidity = detail.DLValidity;                
                dto.DriverId = detail.DriverId;
                dto.DriverName = detail.DriverName;
                dto.DriverPhoto = detail.DriverPhoto;                
                dto.Email = detail.Email;
                dto.FC_Photo = detail.FC_Photo;
                dto.Mobile = detail.Mobile;
                dto.PanCard = detail.PanCard;                
                //dto.Photo = detail.Photo;
                //dto.PhotoContent = detail.PhotoContent;
                //dto.PhotoFileName = detail.PhotoFileName;
                //dto.PhotoType = detail.PhotoType;
                //if (detail.StateDetail != null)
                //{
                //    dto.StateDTO = StateDetailsWrapper(detail.StateDetail);
                //}
                dto.State = detail.State;

                dto.CRDTM = detail.CRDTM;
                dto.IsActive = detail.IsActive;
                dto.IsDelete = detail.IsDelete;
                dto.UPDTM = detail.UPDTM;
            }
            return dto;
        }

        public static DriverDetail DriverDTOWrapper(DriverDTO dto)
        {
            DriverDetail detail = null;
            if (dto != null)
            {
                detail = new DriverDetail();
                detail.AadharCard = dto.AadharCard;
                detail.Address = dto.Address;
                detail.Age = dto.Age;
                detail.City = dto.City;
                //if (dto.CountryDTO != null)
                //{
                //    detail.CountryDetail = CountryDTOWrapper(dto.CountryDTO);
                //}
                detail.Country = dto.Country;
                detail.District = dto.District;
                detail.DLNumber = dto.DLNumber;
                detail.DLValidity = dto.DLValidity;
                detail.DriverId = dto.DriverId;
                detail.DriverName = dto.DriverName;
                detail.DriverPhoto = dto.DriverPhoto;
                detail.Email = dto.Email;
                detail.FC_Photo = dto.FC_Photo;
                detail.Mobile = dto.Mobile;
                detail.PanCard = dto.PanCard;
                //if (dto.StateDTO != null)
                //{
                //    detail.StateDetail = StateDTOWrapper(dto.StateDTO); 
                //}
                detail.State = dto.State;

                detail.CRDTM = dto.CRDTM;
                detail.IsActive = dto.IsActive;
                detail.IsDelete = dto.IsDelete;
                detail.UPDTM = dto.UPDTM;
            }
            return detail;
        }

        public static List<DriverDTO> ListDriverDetailsWrapper(List<DriverDetail> lstdetail)
        {
            List<DriverDTO> lstDTO = null;
            if (lstdetail != null)
            {
                lstDTO = new List<DriverDTO>();
                foreach (var item in lstdetail)
                {
                    DriverDTO dto = DriverDetailsWrapper(item);
                    lstDTO.Add(dto);
                }
            }
            return lstDTO;
        }
        #endregion

        #region GoodsTypeDTO

        public static GoodsTypeDTO GoodsTypeDetailMapper(GoodsTypeDetail detail)
        {
            GoodsTypeDTO dto = null;
            if (detail != null)
            {
                dto = new GoodsTypeDTO();
                dto.GoodsTypeId = detail.GoodsTypeId;
                dto.GoodsTypeName = detail.GoodsTypeName;
            }
            return dto;
        }

        public static GoodsTypeDetail GoodsTypeDTOMapper(GoodsTypeDTO dto)
        {
            GoodsTypeDetail detail = null;
            if (true)
            {
                detail = new GoodsTypeDetail();
                detail.GoodsTypeId = dto.GoodsTypeId;
                detail.GoodsTypeName = dto.GoodsTypeName;
            }
            return detail;
        }

        public static List<GoodsTypeDTO> ListGoodsTypeDetailMapper(List<GoodsTypeDetail> lstDetails)
        {
            List<GoodsTypeDTO> lstDTO = new List<GoodsTypeDTO>();
            foreach (GoodsTypeDetail item in lstDetails)
            {
                GoodsTypeDTO dto = GoodsTypeDetailMapper(item);
                lstDTO.Add(dto);
            }
            return lstDTO;
        }

        #endregion

        #region PartnerDTO
        public static PartnerDTO PartnerDetailsWrapper(PartnerDetail detail)
        {
            PartnerDTO dto = null;
            if (detail != null)
            {
                dto = new PartnerDTO();
                dto.Address = detail.Address;
                dto.Age = detail.Age;
                if (detail.CompanyDetail != null)
                {
                    dto.CompanyDTO = CompanyDetailsWrapper(detail.CompanyDetail);
                }
                dto.CompanyId = detail.CompanyId;
                dto.CRDTM = detail.CRDTM;                
                dto.IsActive = detail.IsActive;
                dto.IsDelete = detail.IsDelete;
                dto.Name = detail.Name;
                dto.PartnerId = detail.PartnerId;             
                dto.UPDTM = detail.UPDTM;
                if (detail.UserDetail != null)
                {
                    dto.UserDTO = UserDetailMapper(detail.UserDetail);
                }
                dto.UserId = detail.UserId;
            }
            return dto;
        }

        public static PartnerDetail PartnerDTOWrapper(PartnerDTO dto)
        {
            PartnerDetail detail = null;
            if (dto != null)
            {
                detail = new PartnerDetail();
                detail.Address = dto.Address;
                detail.Age = dto.Age;
                if (dto.CompanyDTO != null)
                {
                    detail.CompanyDetail = CompanyDTOWrapper(dto.CompanyDTO);
                }
                detail.CompanyId = dto.CompanyId;
                detail.CRDTM = dto.CRDTM;
                detail.IsActive = dto.IsActive;
                detail.IsDelete = dto.IsDelete;
                detail.Name = dto.Name;
                detail.PartnerId = dto.PartnerId;
                detail.UPDTM = dto.UPDTM;
                if (dto.UserDTO != null)
                {
                    detail.UserDetail = UserDTOMapper(dto.UserDTO);
                }
                detail.UserId = dto.UserId;
            }
            return detail;
        }

        public static List<PartnerDTO> ListPartnerDetailsWrapper(List<PartnerDetail> lstdetail)
        {
            List<PartnerDTO> lstDTO = null;
            if (lstdetail != null)
            {
                lstDTO = new List<PartnerDTO>();
                foreach (var item in lstdetail)
                {
                    PartnerDTO dto = PartnerDetailsWrapper(item);
                    lstDTO.Add(dto);
                }
            }
            return lstDTO;
        }
        #endregion

        #region PersonalDTO
        public static PersonalDTO PersonalDetailsWrapper(PersonalDetail detail)
        {
            PersonalDTO dto = null;
            if (detail != null)
            {
                dto = new PersonalDTO();
                dto.AadharCardNumber = detail.AadharCardNumber;
                dto.Address = detail.Address;
                dto.Age = detail.Age;                
                dto.CRDTM = detail.CRDTM;
                dto.EmailId = detail.EmailId;                
                dto.IsActive = detail.IsActive;
                dto.IsDelete = detail.IsDelete;
                dto.MobileNumber = detail.MobileNumber;
                dto.PanCard = detail.PanCard;
                dto.PersonalId = detail.PersonalId;                
                dto.UPDTM = detail.UPDTM;
                if (detail.UserDetail != null)
                {
                    dto.UserDTO = UserDetailMapper(detail.UserDetail);
                }
                dto.UserId = detail.UserId;
                dto.City = detail.City;
            }
            return dto;
        }

        public static PersonalDetail PersonalDTOWrapper(PersonalDTO dto)
        {
            PersonalDetail detail = null;
            if (dto != null)
            {
                detail = new PersonalDetail();
                detail.AadharCardNumber = dto.AadharCardNumber;
                detail.Address = dto.Address;
                detail.Age = dto.Age;
                detail.CRDTM = dto.CRDTM;
                detail.EmailId = dto.EmailId;
                detail.IsActive = dto.IsActive;
                detail.IsDelete = dto.IsDelete;
                detail.MobileNumber = dto.MobileNumber;
                detail.PanCard = dto.PanCard;
                detail.PersonalId = dto.PersonalId;
                detail.UPDTM = dto.UPDTM;
                if (dto.UserDTO != null)
                {
                    detail.UserDetail = UserDTOMapper(dto.UserDTO);
                }
                detail.UserId = dto.UserId;
                detail.City = dto.City;
            }
            return detail;
        }

        public static List<PersonalDTO> ListPersonalDetailsWrapper(List<PersonalDetail> lstdetail)
        {
            List<PersonalDTO> lstDTO = null;
            if (lstdetail != null)
            {
                lstDTO = new List<PersonalDTO>();
                foreach (var item in lstdetail)
                {
                    PersonalDTO dto = PersonalDetailsWrapper(item);
                    lstDTO.Add(dto);
                }
            }
            return lstDTO;
        }
        #endregion

        #region PinCodeDTO
        public static PinCodeDTO PinCodeDetailsWrapper(PinCodeDetail detail)
        {
            PinCodeDTO dto = null;
            if (detail != null)
            {
                dto = new PinCodeDTO();
                dto.CRDTM = detail.CRDTM;
                dto.IsActive = detail.IsActive;
                dto.IsDelete = detail.IsDelete;
                dto.PinCode = detail.PinCode;
                dto.PinCodeId = detail.PinCodeId;
                dto.PinCodeLocation = detail.PinCodeLocation;
                if (detail.RTODetail != null)
                {
                    dto.RtoDTO = RTODetailsWrapper(detail.RTODetail);
                }
                dto.RTOId = dto.RTOId;
                dto.UPDTM = dto.UPDTM;
            }
            return dto;
        }

        public static PinCodeDetail PinCodeDTOWrapper(PinCodeDTO dto)
        {
            PinCodeDetail detail = null;
            if (dto != null)
            {
                detail = new PinCodeDetail();
                detail.CRDTM = dto.CRDTM;
                detail.IsActive = dto.IsActive;
                detail.IsDelete = dto.IsDelete;
                detail.PinCode = dto.PinCode;
                detail.PinCodeId = dto.PinCodeId;
                detail.PinCodeLocation = dto.PinCodeLocation;
                if (dto.RtoDTO != null)
                {
                    detail.RTODetail = RTODTOWrapper(dto.RtoDTO);
                }
                detail.RTOId = dto.RTOId;
                detail.UPDTM = dto.UPDTM;
            }
            return detail;
        }

        public static List<PinCodeDTO> ListPinCodeDetailsWrapper(List<PinCodeDetail> lstdetail)
        {
            List<PinCodeDTO> lstDTO = null;
            if (lstdetail != null)
            {
                lstDTO = new List<PinCodeDTO>();
                foreach (var item in lstdetail)
                {
                    PinCodeDTO dto = PinCodeDetailsWrapper(item);
                    lstDTO.Add(dto);
                }
            }
            return lstDTO;
        }
        #endregion

        #region RoleDTO
        public static RoleDTO RoleDetailsWrapper(RoleDetail detail)
        {
            RoleDTO dto = null;
            if (detail != null)
            {
                dto = new RoleDTO();
                dto.CRDTM = detail.CRDTM;
                dto.IsActive = detail.IsActive;
                dto.IsDelete = detail.IsDelete;
                dto.IsAdmin = detail.IsAdmin;
                dto.RoleId = detail.RoleId;
                dto.RoleName = detail.RoleName;
                dto.UPDTM = detail.UPDTM;
            }
            return dto;
        }

        public static RoleDetail RoleDTOWrapper(RoleDTO dto)
        {
            RoleDetail detail = null;
            if (detail != null)
            {
                detail = new RoleDetail();
                detail.CRDTM = dto.CRDTM;
                detail.IsActive = dto.IsActive;
                detail.IsDelete = dto.IsDelete;
                detail.IsAdmin = dto.IsAdmin;
                detail.RoleId = dto.RoleId;
                detail.RoleName = dto.RoleName;
                detail.UPDTM = dto.UPDTM;
            }
            return detail;
        }

        public static List<RoleDTO> ListRoleDetailsWrapper(List<RoleDetail> lstdetail)
        {
            List<RoleDTO> lstDTO = null;
            if (lstdetail != null)
            {
                lstDTO = new List<RoleDTO>();
                foreach (var item in lstdetail)
                {
                    RoleDTO dto = RoleDetailsWrapper(item);
                    lstDTO.Add(dto);
                }
            }
            return lstDTO;
        }
        #endregion

        #region RtoDTO
        public static RtoDTO RTODetailsWrapper(RTODetail detail)
        {
            RtoDTO dto = null;
            if (detail != null)
            {
                dto = new RtoDTO();
                dto.CRDTM = detail.CRDTM;
                if (detail.DistrictDetail != null)
                {
                    dto.DistrictDTO = DistrictDetailsWrapper(detail.DistrictDetail);
                }
                dto.DistrictId = detail.DistrictId;                
                dto.IsActive = detail.IsActive;
                dto.IsDelete = detail.IsDelete;
                dto.RTOCode = detail.RTOCode;
                dto.RTOId = detail.RTOId;
                dto.RTOLocation = detail.RTOLocation;                
                dto.UPDTM = detail.UPDTM;
            }
            return dto;
        }

        public static RTODetail RTODTOWrapper(RtoDTO dto)
        {
            RTODetail detail = null;
            if (dto != null)
            {
                detail = new RTODetail();
                detail.CRDTM = dto.CRDTM;
                if (dto.DistrictDTO != null)
                {
                    detail.DistrictDetail = DistrictDTOWrapper(dto.DistrictDTO);
                }
                detail.DistrictId = dto.DistrictId;
                detail.IsActive = dto.IsActive;
                detail.IsDelete = dto.IsDelete;
                detail.RTOCode = dto.RTOCode;
                detail.RTOId = dto.RTOId;
                detail.RTOLocation = dto.RTOLocation;
                detail.UPDTM = dto.UPDTM;
            }
            return detail;
        }

        public static List<RtoDTO> ListRTODetailsWrapper(List<RTODetail> lstdetail)
        {
            List<RtoDTO> lstDTO = null;
            if (lstdetail != null)
            {
                lstDTO = new List<RtoDTO>();
                foreach (var item in lstdetail)
                {
                    RtoDTO dto = RTODetailsWrapper(item);
                    lstDTO.Add(dto);
                }
            }
            return lstDTO;
        }
        #endregion

        #region StateDTO
        public static StateDTO StateDetailsWrapper(StateDetail detail)
        {
            StateDTO dto = null;
            if (detail != null)
            {
                dto = new StateDTO();
                if (dto.CountryDTO != null)
                {
                    dto.CountryDTO = CountryDetailsWrapper(detail.CountryDetail);
                }
                dto.CountryId = detail.CountryId;
                dto.CRDTM = detail.CRDTM;
                dto.IsActive = detail.IsActive;
                dto.IsDelete = detail.IsDelete;
                dto.StateId = detail.StateId;
                dto.StateName = detail.StateName;
                dto.UPDTM = detail.UPDTM;
            }
            return dto;
        }

        public static StateDetail StateDTOWrapper(StateDTO dto)
        {
            StateDetail detail = null;
            if (dto != null)
            {
                detail = new StateDetail();
                if (dto.CountryDTO != null)
                {
                    detail.CountryDetail = CountryDTOWrapper(dto.CountryDTO);
                }
                detail.CountryId = dto.CountryId;
                detail.CRDTM = dto.CRDTM;
                detail.IsActive = dto.IsActive;
                detail.IsDelete = dto.IsDelete;
                detail.StateId = dto.StateId;
                detail.StateName = dto.StateName;
                detail.UPDTM = dto.UPDTM;
            }
            return detail;
        }

        public static List<StateDTO> ListStateDetailsWrapper(List<StateDetail> lstdetail)
        {
            List<StateDTO> lstDTO = null;
            if (lstdetail != null)
            {
                lstDTO = new List<StateDTO>();
                foreach (var item in lstdetail)
                {
                    StateDTO dto = StateDetailsWrapper(item);
                    lstDTO.Add(dto);
                }
            }
            return lstDTO;
        }
        #endregion

        #region TicketDTO
        public static TicketDTO TicketDetailsWrapper(TicketDetail detail)
        {
            TicketDTO dto = null;
            if (detail != null)
            {
                dto = new TicketDTO();
                dto.BAComments = detail.BAComments;
                dto.BAPermission = detail.BAPermission;
                dto.CRDTM = detail.CRDTM;
                //dto.CreatedDate = detail.CreatedDate;                
                
                dto.DHComments = detail.DHComments;
                dto.DHPermission = detail.DHPermission;
                if (detail.DriverDetail != null)
	            {
                    dto.DriverDTO = DriverDetailsWrapper(detail.DriverDetail);
	            }
                dto.DriverId = detail.DriverId;
                if (detail.GoodsTypeDetail != null)
                {
                    dto.GoodsTypeDTO = GoodsTypeDetailMapper(detail.GoodsTypeDetail);
                }
                dto.GoodsTypeId = detail.GoodsTypeId;
                dto.IsActive = detail.IsActive;
                dto.IsDelete = detail.IsDelete;
                //dto.GoodsType = detail.GoodsType;
                dto.RenewalCounter = detail.RenewalCounter;
                dto.SAComments = detail.SAComments;
                dto.SAPermission = detail.SAPermission;
                dto.SHComments = detail.SHComments;
                dto.SHPermission = detail.SHPermission;
                dto.TicketEndDate = detail.TicketEndDate;
                dto.TicketId = detail.TicketId;
                dto.TicketStartDate = detail.TicketStartDate;
                if (detail.TonnageDetail != null)
                {
                    dto.TonnageDTO = TonnageDetailsWrapper(detail.TonnageDetail);
                }
                dto.TonnageId = detail.TonnageId;
                dto.UPDTM = detail.UPDTM;
                if (detail.UserDetail != null)
                {
                    dto.UserDTO = UserDetailMapper(detail.UserDetail);
                }
                dto.UserId = detail.UserId;
                //dto.ValidityFrom = detail.ValidityFrom;
                if (detail.VehicleDetail != null)
                {
                    dto.VehicleDTO = VehicleDetailsWrapper(detail.VehicleDetail);
                }
                dto.VehicleId = detail.VehicleId;
                dto.TicketStatusId = detail.TicketStatusId;
                dto.Amount = detail.Amount;

                dto.Address = detail.Address;
                dto.City = detail.City;
                dto.District = detail.District;
                dto.State = detail.State;
                dto.Country = detail.Country;
            }
            return dto;
        }

        public static TicketDetail TicketDTOWrapper(TicketDTO dto)
        {
            TicketDetail detail = null;
            if (dto != null)
            {
                detail = new TicketDetail();
                detail.BAComments = dto.BAComments;
                detail.BAPermission = dto.BAPermission;
                detail.CRDTM = dto.CRDTM;
                //detail.CreatedDate = dto.CreatedDate;

                detail.DHComments = dto.DHComments;
                detail.DHPermission = dto.DHPermission;
                if (dto.DriverDTO != null)
                {
                    detail.DriverDetail = DriverDTOWrapper(dto.DriverDTO);
                }
                detail.DriverId = dto.DriverId;
                if (dto.GoodsTypeDTO != null)
	            {
                    detail.GoodsTypeDetail = GoodsTypeDTOMapper(dto.GoodsTypeDTO);
	            }
                detail.GoodsTypeId = dto.GoodsTypeId;
                detail.IsActive = dto.IsActive;
                detail.IsDelete = dto.IsDelete;
                //detail.GoodsType = dto.GoodsType;
                detail.RenewalCounter = dto.RenewalCounter;
                detail.SAComments = dto.SAComments;
                detail.SAPermission = dto.SAPermission;
                detail.SHComments = dto.SHComments;
                detail.SHPermission = dto.SHPermission;
                detail.TicketEndDate = dto.TicketEndDate;
                detail.TicketId = dto.TicketId;
                detail.TicketStartDate = dto.TicketStartDate;
                if (dto.TonnageDTO != null)
                {
                    detail.TonnageDetail = TonnageDTOWrapper(dto.TonnageDTO);
                }
                detail.TonnageId = dto.TonnageId;
                detail.UPDTM = dto.UPDTM;
                if (dto.UserDTO != null)
                {
                    detail.UserDetail = UserDTOMapper(dto.UserDTO);
                }
                detail.UserId = dto.UserId;
                //detail.ValidityFrom = dto.ValidityFrom;
                if (dto.VehicleDTO != null)
                {
                    detail.VehicleDetail = VehicleDTOWrapper(dto.VehicleDTO);
                }
                detail.VehicleId = dto.VehicleId;
                detail.TicketStatusId = dto.TicketStatusId;
                detail.Amount = dto.Amount;

                detail.Address = dto.Address;
                detail.City = dto.City;
                detail.District = dto.District;
                detail.State = dto.State;
                detail.Country = dto.Country;
            }
            return detail;
        }

        public static List<TicketDTO> ListTicketDetailsWrapper(List<TicketDetail> lstdetail)
        {
            List<TicketDTO> lstDTO = null;
            if (lstdetail != null)
            {
                lstDTO = new List<TicketDTO>();
                foreach (var item in lstdetail)
                {
                    TicketDTO dto = TicketDetailsWrapper(item);
                    lstDTO.Add(dto);
                }
            }
            return lstDTO;
        }
        #endregion

        #region TicketStatusDTO

        public static TicketStatusDTO TicketStatusDetailMapper(TicketStatusDetail detail)
        {
            TicketStatusDTO dto = null;
            if (detail != null)
            {
                dto = new TicketStatusDTO();
                dto.TicketStatusId = detail.TicketStatusId;
                dto.TicketStatusDesc = detail.TicketStatusDesc;
            }
            return dto;
        }

        public static TicketStatusDetail TicketStatusDTOMapper(TicketStatusDTO dto)
        {
            TicketStatusDetail detail = null;
            if (true)
            {
                detail = new TicketStatusDetail();
                detail.TicketStatusId = dto.TicketStatusId;
                detail.TicketStatusDesc = dto.TicketStatusDesc;
            }
            return detail;
        }

        public static List<TicketStatusDTO> ListTicketStatusDetailMapper(List<TicketStatusDetail> lstDetails)
        {
            List<TicketStatusDTO> lstDTO = new List<TicketStatusDTO>();
            foreach (TicketStatusDetail item in lstDetails)
            {
                TicketStatusDTO dto = TicketStatusDetailMapper(item);
                lstDTO.Add(dto);
            }
            return lstDTO;
        }

        #endregion

        #region TonnageDTO
        public static TonnageDTO TonnageDetailsWrapper(TonnageDetail detail)
        {
            TonnageDTO dto = null;
            if (detail != null)
            {
                dto = new TonnageDTO();
                dto.CRDTM = detail.CRDTM;
                dto.IsActive = detail.IsActive;
                dto.IsDelete = detail.IsDelete;
                dto.TonnageCost = detail.TonnageCost;
                dto.TonnageFrom = detail.TonnageFrom;
                dto.TonnageId = detail.TonnageId;
                dto.TonnageTo = detail.TonnageTo;
                dto.UPDTM = detail.UPDTM;
            }
            return dto;
        }

        public static TonnageDetail TonnageDTOWrapper(TonnageDTO dto)
        {
            TonnageDetail detail = null;
            if (dto != null)
            {
                detail = new TonnageDetail();
                detail.CRDTM = dto.CRDTM;
                detail.IsActive = dto.IsActive;
                detail.IsDelete = dto.IsDelete;
                detail.TonnageCost = dto.TonnageCost;
                detail.TonnageFrom = dto.TonnageFrom;
                detail.TonnageId = dto.TonnageId;
                detail.TonnageTo = dto.TonnageTo;
                detail.UPDTM = dto.UPDTM;
            }
            return detail;
        }

        public static List<TonnageDTO> ListTonnageDetailsWrapper(List<TonnageDetail> lstdetail)
        {
            List<TonnageDTO> lstDTO = null;
            if (lstdetail != null)
            {
                lstDTO = new List<TonnageDTO>();
                foreach (var item in lstdetail)
                {
                    TonnageDTO dto = TonnageDetailsWrapper(item);
                    lstDTO.Add(dto);
                }
            }
            return lstDTO;
        }
        #endregion

        #region UserDTO
        public static UserDTO UserDetailMapper(UserDetail userDetail)
        {
            //UserDTO userDTO = JsonConvert.DeserializeObject<UserDTO>(JsonConvert.SerializeObject(userDetail));
            UserDTO userDTO = null;
            if (userDetail != null)
            {
                userDTO = new UserDTO();
                userDTO.UserId = userDetail.UserId;
                userDTO.Name = userDetail.Name;
                userDTO.Password = userDetail.Password;
                userDTO.Status = userDetail.Status;
                userDTO.RoleId = userDetail.RoleId;
                userDTO.PinCodeId = userDetail.PinCodeId;
                userDTO.RTOId = userDetail.RTOId;
                userDTO.DistrictId = userDetail.DistrictId;
                userDTO.StateId = userDetail.StateId;
                userDTO.CountryId = userDetail.CountryId;
                userDTO.CRDTM = userDetail.CRDTM;
                userDTO.UPDTM = userDetail.UPDTM;
                userDTO.IsActive = userDetail.IsActive;
                userDTO.IsDelete = userDetail.IsDelete;
            }             
            return userDTO;
        }

        public static UserDetail UserDTOMapper(UserDTO userDTO)
        {
            //UserDTO userDTO = JsonConvert.DeserializeObject<UserDTO>(JsonConvert.SerializeObject(userDetail));
            UserDetail userDetail = null;
            if (userDTO != null)
            {
                userDetail = new UserDetail();
                userDetail.UserId = userDTO.UserId;
                userDetail.Name = userDTO.Name;
                userDetail.Password = userDTO.Password;
                userDetail.Status = userDTO.Status;
                userDetail.RoleId = userDTO.RoleId;
                userDetail.PinCodeId = userDTO.PinCodeId;
                userDetail.RTOId = userDTO.RTOId;
                userDetail.DistrictId = userDTO.DistrictId;
                userDetail.StateId = userDTO.StateId;
                userDetail.CountryId = userDTO.CountryId;
                userDetail.CRDTM = userDTO.CRDTM;
                userDetail.UPDTM = userDTO.UPDTM;
                userDetail.IsActive = userDTO.IsActive;
                userDetail.IsDelete = userDTO.IsDelete;
            }
            return userDetail;
        }

        public static List<UserDTO> ListUserDetailMapper(List<UserDetail> lstUserDetails)
        {
            List<UserDTO> lstUserDTO = null;
            if (lstUserDetails != null)
            {
                lstUserDTO = new List<UserDTO>();
                foreach (UserDetail item in lstUserDetails)
                {
                    UserDTO userDTO = UserDetailMapper(item);
                    lstUserDTO.Add(userDTO);
                }
            }            
            return lstUserDTO;
        }
        #endregion

        #region VehicleDTO
        public static VehicleDTO VehicleDetailsWrapper(VehicleDetail detail)
        {
            VehicleDTO dto = null;
            if (detail != null)
            {
                dto = new VehicleDTO();
                dto.Breadth = detail.Breadth;
                dto.ChasisNumber = detail.ChasisNumber;
                dto.CubicCapacity = detail.CubicCapacity;
                dto.EngineNumber = detail.EngineNumber;
                dto.FCValidityFrom = detail.FCValidityFrom;
                dto.FCValidityTo = detail.FCValidityTo;
                dto.Height = detail.Height;
                dto.InsurancePhoto = detail.InsurancePhoto;
                dto.InsuranceEndDate = detail.InsuranceEndDate;
                dto.Length = detail.Length;                
                dto.Make = detail.Make;
                dto.Model = detail.Model;
                dto.NOCPolicy = detail.NOCPolicy;
                dto.NOCRTO = detail.NOCRTO;
                dto.PermitEndDate = detail.PermitEndDate;
                dto.PermitPhoto = detail.PermitPhoto;
                dto.RCBookPhoto = detail.RCBookPhoto;
                //dto.Photo = detail.Photo;
                //dto.PhotoContent = detail.PhotoContent;
                //dto.PhotoFileName = detail.PhotoFileName;
                //dto.PhotoType = detail.PhotoType;
                dto.Registration = detail.Registration;                
                dto.VehicleId = detail.VehicleId;
                if (detail.VehicleOwnerDetail != null)
                {
                    dto.VehicleOwnerDTO = VehicleOwnerDetailsWrapper(detail.VehicleOwnerDetail);
                }
                dto.VehicleOwnerId = detail.VehicleOwnerId;
                dto.VehicleStatus = detail.VehicleStatus;
                dto.Width = detail.Width;
                dto.Year = detail.Year;
                dto.CRDTM = detail.CRDTM;
                dto.IsActive = detail.IsActive;
                dto.IsDelete = detail.IsDelete;
                dto.UPDTM = detail.UPDTM;

                dto.VehiclePhoto1 = detail.VehiclePhoto1;
                dto.VehiclePhoto2 = detail.VehiclePhoto2;
                dto.VehiclePhoto3 = detail.VehiclePhoto3;
                dto.VehiclePhoto4 = detail.VehiclePhoto4;
                dto.InsuranceStartDate = detail.InsuranceStartDate;
            }
            return dto;
        }

        public static VehicleDetail VehicleDTOWrapper(VehicleDTO dto)
        {
            VehicleDetail detail = null;
            if (dto != null)
            {
                detail = new VehicleDetail();
                detail.Breadth = dto.Breadth;
                detail.ChasisNumber = dto.ChasisNumber;
                detail.CubicCapacity = dto.CubicCapacity;
                detail.EngineNumber = dto.EngineNumber;
                detail.FCValidityFrom = dto.FCValidityFrom;
                detail.FCValidityTo = dto.FCValidityTo;
                detail.Height = dto.Height;
                detail.InsurancePhoto = dto.InsurancePhoto;
                detail.InsuranceEndDate = dto.InsuranceEndDate;
                detail.Length = dto.Length;
                detail.Make = dto.Make;
                detail.Model = dto.Model;
                detail.NOCPolicy = dto.NOCPolicy;
                detail.NOCRTO = dto.NOCRTO;
                detail.PermitEndDate = dto.PermitEndDate;
                detail.PermitPhoto = dto.PermitPhoto;
                detail.RCBookPhoto = dto.RCBookPhoto;
                //detail.Photo = dto.Photo;
                //detail.PhotoContent = dto.PhotoContent;
                //detail.PhotoFileName = dto.PhotoFileName;
                //detail.PhotoType = dto.PhotoType;
                detail.Registration = dto.Registration;
                detail.VehicleId = dto.VehicleId;
                if (dto.VehicleOwnerDTO != null)
                {
                    detail.VehicleOwnerDetail = VehicleOwnerDTOWrapper(dto.VehicleOwnerDTO);
                }
                detail.VehicleOwnerId = dto.VehicleOwnerId;
                detail.VehicleStatus = dto.VehicleStatus;
                detail.Width = dto.Width;
                detail.Year = dto.Year;
                detail.CRDTM = dto.CRDTM;
                detail.IsActive = dto.IsActive;
                detail.IsDelete = dto.IsDelete;
                detail.UPDTM = dto.UPDTM;

                detail.VehiclePhoto1 = dto.VehiclePhoto1;
                detail.VehiclePhoto2 = dto.VehiclePhoto2;
                detail.VehiclePhoto3 = dto.VehiclePhoto3;
                detail.VehiclePhoto4 = dto.VehiclePhoto4;
                
                detail.InsuranceStartDate = dto.InsuranceStartDate;
            }
            return detail;
        }

        public static List<VehicleDTO> ListVehicleDetailsWrapper(List<VehicleDetail> lstdetail)
        {
            List<VehicleDTO> lstDTO = null;
            if (lstdetail != null)
            {
                lstDTO = new List<VehicleDTO>();
                foreach (var item in lstdetail)
                {
                    VehicleDTO dto = VehicleDetailsWrapper(item);
                    lstDTO.Add(dto);
                }
            }
            return lstDTO;
        }
        #endregion

        #region VehicleLocationDTO
        public static VehicleLocationDTO VehicleLocationDetailsWrapper(VehicleLocationDetail detail)
        {
            VehicleLocationDTO dto = null;
            if (detail != null)
            {
                dto = new VehicleLocationDTO();
                dto.Address = detail.Address;
                dto.City = detail.City;
                if (detail.CountryDetail != null)
                {
                    dto.CountryDTO = CountryDetailsWrapper(detail.CountryDetail);
                }
                dto.CountryId = detail.CountryId;
                dto.CRDTM = detail.CRDTM;
                if (detail.DistrictDetail != null)
                {
                    dto.DistrictDTO = DistrictDetailsWrapper(detail.DistrictDetail);
                }
                dto.DistrictId = detail.DistrictId;                
                dto.IsActive = detail.IsActive;
                dto.IsDelete = detail.IsDelete;
                dto.Location = detail.Location;
                if (detail.StateDetail != null)
                {
                    dto.StateDTO = StateDetailsWrapper(detail.StateDetail);
                }
                dto.StateId = detail.StateId;                
                dto.UPDTM = detail.UPDTM;
                if (detail.VehicleDetail != null)
                {
                    dto.VehicleDTO = VehicleDetailsWrapper(detail.VehicleDetail);
                }
                dto.VehicleId = detail.VehicleId;
                dto.VehicleLocationId = detail.VehicleLocationId;
            }
            return dto;
        }

        public static VehicleLocationDetail VehicleLocationDTOWrapper(VehicleLocationDTO dto)
        {
            VehicleLocationDetail detail = null;
            if (dto != null)
            {
                detail = new VehicleLocationDetail();
                detail.Address = dto.Address;
                detail.City = dto.City;
                if (dto.CountryDTO != null)
                {
                    detail.CountryDetail = CountryDTOWrapper(dto.CountryDTO);
                }
                detail.CountryId = dto.CountryId;
                detail.CRDTM = dto.CRDTM;
                if (dto.DistrictDTO != null)
                {
                    detail.DistrictDetail = DistrictDTOWrapper(dto.DistrictDTO);
                }
                detail.DistrictId = dto.DistrictId;
                detail.IsActive = dto.IsActive;
                detail.IsDelete = dto.IsDelete;
                detail.Location = dto.Location;
                if (dto.StateDTO != null)
                {
                    detail.StateDetail = StateDTOWrapper(dto.StateDTO);
                }
                detail.StateId = dto.StateId;
                detail.UPDTM = dto.UPDTM;
                if (dto.VehicleDTO != null)
                {
                    detail.VehicleDetail = VehicleDTOWrapper(dto.VehicleDTO);
                }
                detail.VehicleId = dto.VehicleId;
                detail.VehicleLocationId = dto.VehicleLocationId;
            }
            return detail;
        }

        public static List<VehicleLocationDTO> ListVehicleLocationDetailsWrapper(List<VehicleLocationDetail> lstdetail)
        {
            List<VehicleLocationDTO> lstDTO = null;
            if (lstdetail != null)
            {
                lstDTO = new List<VehicleLocationDTO>();
                foreach (var item in lstdetail)
                {
                    VehicleLocationDTO dto = VehicleLocationDetailsWrapper(item);
                    lstDTO.Add(dto);
                }
            }
            return lstDTO;
        }
        #endregion

        #region VehicleOwnerDTO
        public static VehicleOwnerDTO VehicleOwnerDetailsWrapper(VehicleOwnerDetail detail)
        {
            VehicleOwnerDTO dto = null;
            if (detail != null)
            {
                dto = new VehicleOwnerDTO();
                dto.AadharCard = detail.AadharCard;
                dto.Address = detail.Address;
                dto.Age = detail.Age;
                dto.City = detail.City;
                if (detail.CountryDetail != null)
                {
                    dto.CountryDTO = CountryDetailsWrapper(detail.CountryDetail);
                }
                dto.CountryId = detail.CountryId;
                dto.District = detail.District;
                dto.Email = detail.Email;
                dto.Mobile = detail.Mobile;
                dto.PAN = detail.PAN;
                dto.Pincode = detail.Pincode;
                if (detail.StateDetail != null)
                {
                    dto.StateDTO = StateDetailsWrapper(detail.StateDetail);
                }
                dto.StateId = detail.StateId;
                dto.VehicleOwnerId = detail.VehicleOwnerId;
                dto.VehicleOwnerName = detail.VehicleOwnerName;
                dto.CRDTM = detail.CRDTM;
                dto.IsActive = detail.IsActive;
                dto.IsDelete = detail.IsDelete;
                dto.UPDTM = detail.UPDTM;
            }
            return dto;
        }

        public static VehicleOwnerDetail VehicleOwnerDTOWrapper(VehicleOwnerDTO dto)
        {
            VehicleOwnerDetail detail = null;
            if (dto != null)
            {
                detail = new VehicleOwnerDetail();
                detail.AadharCard = dto.AadharCard;
                detail.Address = dto.Address;
                detail.Age = dto.Age;
                detail.City = dto.City;
                if (dto.CountryDTO != null)
                {
                    detail.CountryDetail = CountryDTOWrapper(dto.CountryDTO);
                }
                detail.CountryId = dto.CountryId;
                detail.District = dto.District;
                detail.Email = dto.Email;
                detail.Mobile = dto.Mobile;
                detail.PAN = dto.PAN;
                detail.Pincode = dto.Pincode;
                if (dto.StateDTO != null)
                {
                    detail.StateDetail = StateDTOWrapper(dto.StateDTO);
                }
                detail.StateId = dto.StateId;
                detail.VehicleOwnerId = dto.VehicleOwnerId;
                detail.VehicleOwnerName = dto.VehicleOwnerName;
                detail.CRDTM = dto.CRDTM;
                detail.IsActive = dto.IsActive;
                detail.IsDelete = dto.IsDelete;
                detail.UPDTM = dto.UPDTM;
            }
            return detail;
        }

        public static List<VehicleOwnerDTO> ListVehicleOwnerDetailsWrapper(List<VehicleOwnerDetail> lstdetail)
        {
            List<VehicleOwnerDTO> lstDTO = null;
            if (lstdetail != null)
            {
                lstDTO = new List<VehicleOwnerDTO>();
                foreach (var item in lstdetail)
                {
                    VehicleOwnerDTO dto = VehicleOwnerDetailsWrapper(item);
                    lstDTO.Add(dto);
                }
            }
            return lstDTO;
        }
        #endregion
    }
}