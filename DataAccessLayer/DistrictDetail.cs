//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class DistrictDetail
    {
        public DistrictDetail()
        {
            this.RTODetails = new HashSet<RTODetail>();
            this.UserDetails = new HashSet<UserDetail>();
            this.VehicleLocationDetails = new HashSet<VehicleLocationDetail>();
        }
    
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int StateId { get; set; }
        public System.DateTime CRDTM { get; set; }
        public System.DateTime UPDTM { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
    
        public virtual StateDetail StateDetail { get; set; }
        public virtual ICollection<RTODetail> RTODetails { get; set; }
        public virtual ICollection<UserDetail> UserDetails { get; set; }
        public virtual ICollection<VehicleLocationDetail> VehicleLocationDetails { get; set; }
    }
}
