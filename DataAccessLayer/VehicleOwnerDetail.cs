//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class VehicleOwnerDetail
    {
        public VehicleOwnerDetail()
        {
            this.VehicleDetails = new HashSet<VehicleDetail>();
        }
    
        public int VehicleOwnerId { get; set; }
        public string VehicleOwnerName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public int Pincode { get; set; }
        public string District { get; set; }
        public int StateId { get; set; }
        public int CountryId { get; set; }
        public int Age { get; set; }
        public long Mobile { get; set; }
        public string Email { get; set; }
        public string PAN { get; set; }
        public string AadharCard { get; set; }
        public System.DateTime CRDTM { get; set; }
        public System.DateTime UPDTM { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
    
        public virtual CountryDetail CountryDetail { get; set; }
        public virtual StateDetail StateDetail { get; set; }
        public virtual ICollection<VehicleDetail> VehicleDetails { get; set; }
    }
}
