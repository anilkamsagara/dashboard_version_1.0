//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccessLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class RTODetail
    {
        public RTODetail()
        {
            this.PinCodeDetails = new HashSet<PinCodeDetail>();
            this.UserDetails = new HashSet<UserDetail>();
        }
    
        public int RTOId { get; set; }
        public string RTOCode { get; set; }
        public string RTOLocation { get; set; }
        public int DistrictId { get; set; }
        public System.DateTime CRDTM { get; set; }
        public System.DateTime UPDTM { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
    
        public virtual DistrictDetail DistrictDetail { get; set; }
        public virtual ICollection<PinCodeDetail> PinCodeDetails { get; set; }
        public virtual ICollection<UserDetail> UserDetails { get; set; }
    }
}
