﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataTransferObject;
using BusinessManager;

namespace ServiceLayer
{
    public class VehicleService
    {
        public List<VehicleDTO> GetVehiclesListForBA(int userId)
        {
            try
            {
                VehicleManager vManager = new VehicleManager();
                List<VehicleDTO> vehicles = vManager.GetVehiclesListForBA(userId);
                return vehicles;
            }
            catch (Exception)
            {                
                throw;
            }
        }
    }
}
