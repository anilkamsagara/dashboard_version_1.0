﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Repository;
using DataTransferObject;

namespace ServiceLayer
{
    public class PinCodeService
    {
        public List<PinCodeDTO> GetPinCode(int rtoId)
        {
            try
            {
                PinCodeRepository pcRep = new PinCodeRepository();
                return pcRep.GetPinCode(rtoId);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
