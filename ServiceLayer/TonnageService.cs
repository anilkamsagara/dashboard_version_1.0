﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Repository;
using DataTransferObject;

namespace ServiceLayer
{
    public class TonnageService
    {
        TonnageRepository tRep = new TonnageRepository();
        #region "Public Methods"
        public List<TonnageDTO> GetAllTonnageDetails()
        {
            try
            {
                
                List<TonnageDTO> lstTonnage = tRep.GetAll().ToList();
                var tonnage = lstTonnage.Select(t => new TonnageDTO(){
                                                    TonnageId = t.TonnageId,
                                                    TonnageRange = string.Concat(t.TonnageFrom,"-",t.TonnageTo),
                                                    TonnageCost = t.TonnageCost,
                                                    TonnageTo = t.TonnageTo,
                                                    TonnageFrom = t.TonnageFrom
                                                }).ToList();
                return tonnage;
            }
            catch (Exception)
            {                
                throw;
            }
        }

        public double TonnageChange(int tonnageId)
        {
            try
            {
                TonnageDTO tonnage = tRep.GetById(tonnageId);
                double cost = 0.0;
                if (tonnage != null)
                {
                    cost = tonnage.TonnageCost;
                }
                return cost;
            }
            catch (Exception)
            {                
                throw;
            }
        }

        public bool UpdateTonnage(int tonnageId, double cost)
        {
            try
            {
                TonnageDTO dto = FillTonnageDTO(tonnageId,cost);
                return tRep.Update(dto);
            }
            catch (Exception)
            {                
                throw;
            }
        }
        #endregion

        #region "Private Methods"
        private TonnageDTO FillTonnageDTO(int tonnageId, double cost)
        {
            TonnageDTO dto = tRep.GetById(tonnageId);
            dto.IsActive = true;
            dto.IsDelete = false;
            dto.TonnageCost = cost;
            dto.UPDTM = DateTime.UtcNow;
            return dto;
        }
        #endregion
    }
}
