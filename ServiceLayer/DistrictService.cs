﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataTransferObject;
using DataAccessLayer.Repository;

namespace ServiceLayer
{
    public class DistrictService
    {
        public List<DistrictDTO> GetDistricts(int stateId)
        {
            try
            {
                DistrictRepository districtRep = new DistrictRepository();
                return districtRep.GetDistricts(stateId);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
}
