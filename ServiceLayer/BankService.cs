﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Repository;
using DataTransferObject;

namespace ServiceLayer
{
    public class BankService
    {
        public List<BanksInIndiaDTO> GetBanksInIndia()
        {
            try
            {            
                BanksInIndiaRepository banksRep = new BanksInIndiaRepository();
                return banksRep.GetAll().ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
