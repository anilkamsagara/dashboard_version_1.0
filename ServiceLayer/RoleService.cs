﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Repository;
using DataTransferObject;

namespace ServiceLayer
{
    public class RoleService
    {
        /// <summary>
        /// To get All User Role info except ADMIN
        /// </summary>
        /// <returns></returns>
        public List<RoleDTO> GetUserRoles()
        {
            try
            {
                RoleRepository roleRep = new RoleRepository();
                List<RoleDTO> lstRole = roleRep.GetAll().Where(x => !x.IsAdmin).ToList();
                return lstRole;
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// To get Role info based on Role Id
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public RoleDTO GetRoleDetails(int roleId)
        {
            try
            {
                RoleRepository roleRep = new RoleRepository();
                RoleDTO userRole = roleRep.GetAll().Where(r => r.RoleId == roleId).FirstOrDefault();
                return userRole;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
