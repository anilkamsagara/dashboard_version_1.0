﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Repository;
using DataTransferObject;

namespace ServiceLayer
{
    public class RTOService
    {
        public List<RtoDTO> GetRtos(int districtId)
        {
            try
            {            
                RTORepository rtoRep = new RTORepository();
                return rtoRep.GetRtos(districtId);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
