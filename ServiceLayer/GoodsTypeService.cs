﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Repository;
using DataTransferObject;

namespace ServiceLayer
{
    public class GoodsTypeService
    {
        GoodsTypeRepository gRep = new GoodsTypeRepository();
        public List<GoodsTypeDTO> GetAllGoodsType()
        {
            try
            {
                List<GoodsTypeDTO> lstGoodsType = gRep.GetAll().ToList();
                return lstGoodsType;
            }
            catch (Exception)
            {                
                throw;
            }
        }
    }
}
