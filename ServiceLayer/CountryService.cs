﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataTransferObject;
using DataAccessLayer.Repository;

namespace ServiceLayer
{
    public class CountryService
    {
        public List<CountryDTO> GetCountry()
        {
            try
            {
                CountryRepository cRep = new CountryRepository();
                return cRep.GetAll().ToList();
            }
            catch (Exception)
            {                
                throw;
            }           
            
        }
    }
}
