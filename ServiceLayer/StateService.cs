﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Repository;
using DataTransferObject;

namespace ServiceLayer
{
    public class StateService
    {
        public List<StateDTO> GetStates(int countryId)
        {
            try
            {            
                StateRepository stateRep = new StateRepository();
                return stateRep.GetStates(countryId);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
