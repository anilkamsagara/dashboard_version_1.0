﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using BusinessManager;
using DataAccessLayer.Repository;
using DataTransferObject;

namespace ServiceLayer
{
    public class TicketService
    {
        TicketRepository tRep = new TicketRepository();
        public int CreateTicket(TicketDTO ticket, HttpPostedFileBase InsurancePhoto, IEnumerable<HttpPostedFileBase> photo, HttpPostedFileBase RCBookPhoto, HttpPostedFileBase PermitPhoto, HttpPostedFileBase fcPhoto
            , HttpPostedFileBase DriverPhoto, int userId)
        {
            try
            {            
                TicketManager tManager = new TicketManager();
                VehicleRepository vRep = new VehicleRepository();
                DriverRepository dRep = new DriverRepository();                
                AccountRepository aRep = new AccountRepository();
                int result = 0;
                ticket = tManager.CreateTicket(ticket, InsurancePhoto, photo, RCBookPhoto, PermitPhoto, fcPhoto, DriverPhoto);

                
                var vehicle = vRep.GetAll().Where(x => x.Registration == ticket.VehicleDTO.Registration).FirstOrDefault();
                if (vehicle == null)
                {
                    var option = new TransactionOptions();
                    option.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, option))
                    {
                        int vId = vRep.Create(ticket.VehicleDTO);
                        if (vId > 0)
                        {
                            int driverId = dRep.Create(ticket.DriverDTO);
                            if (driverId > 0)
                            {
                                ticket.DriverId = driverId;
                                ticket.VehicleId = vId;
                                ticket.UserId = userId;
                                int ticketId = tRep.Create(ticket);
                                if (ticketId > 0)
                                {
                                    AccountDTO account = aRep.GetByUserId(userId);
                                    if (account != null)
                                    {
                                        account.Funds += ticket.Amount;
                                        aRep.Update(account);
                                        result = -1;
                                        scope.Complete();
                                    }                                    
                                }
                            }
                            else
                            {
                                result = 1; // Driver details insertion Failed
                            }
                        }
                        else
                        {
                            result = 2; // Vehicle details insertion Failed
                        }
                    }
                }
                else
                {
                    result = 3; // Registration No exists
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Get the Tickets created by BA
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<TicketDTO> GetTicketsByUserId(int userId)
        {
            try
            {
                TicketManager tManager = new TicketManager();
                return tManager.GetTicketsByUserId(userId);
            }
            catch (Exception)
            {                
                throw;
            }
        }

        public bool DHApproveReject(int ticketId, string comment, int status)
        {
            try
            {
                TicketManager tManager = new TicketManager();
                return tManager.DHApproveReject( ticketId, comment, status);
            }
            catch (Exception)
            {                
                throw;
            }
        }

        public List<TicketDTO> GetDHTickets(int userId)
        {
            try
            {
                TicketManager tManager = new TicketManager();
                return tManager.GetDHTickets(userId);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
