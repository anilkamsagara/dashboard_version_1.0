﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;
using DataAccessLayer.Repository;
using DataTransferObject;
using System.Transactions;

namespace ServiceLayer
{
    public class UserService
    {
        #region "Public Methods"
        public UserDTO GetLoginUser(string userName, string password)
        {
            try
            {
                UserRepository userRep = new UserRepository();
                return userRep.GetLoginUser(userName, password);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int CreateUser(UserDTO userDTO,PersonalDTO personalDTO,CompanyDTO companyDTO,PartnerDTO partnerDTO,BankDTO bankDTO,AccountDTO accountDTO)
        {
            try
            {                              
                UserRepository userRep = new UserRepository();               
                int result = 0;
                
                if (userDTO != null)
                {
                    var user = userRep.GetByUserName(userDTO.Name);
                    if (user == null)
                    {
                        // Create
                        var lstUser = userRep.GetAllUsers().ToList();
                        if (userDTO.RoleId == Convert.ToInt32(RoleDTO.UserRoles.StateHead))
                        {
                            var stateUser = lstUser.Where(u => u.StateId == userDTO.StateId && u.RoleId == Convert.ToInt32(RoleDTO.UserRoles.StateHead)).FirstOrDefault();
                            if (stateUser != null)
                            {
                                result = 2;//return SH exists
                            }
                            else
                            {
                                userDTO = FillUserDTO(userDTO);
                                personalDTO = FillPersonalDTO(personalDTO);
                                companyDTO = FillCompanyDTO(companyDTO);
                                partnerDTO = FillPartnerDTO(partnerDTO);
                                bankDTO = FillBankDTO(bankDTO);
                                accountDTO = FillAccountDTO(accountDTO);
                                result = InsertUserDetails(userDTO, personalDTO, companyDTO, partnerDTO, bankDTO, accountDTO);
                            }
                        }
                        else if (userDTO.RoleId == Convert.ToInt32(RoleDTO.UserRoles.DistrictHead))
                        {
                            var distUser = lstUser.Where(u => u.StateId == userDTO.StateId && u.DistrictId == userDTO.DistrictId && u.RoleId == Convert.ToInt32(RoleDTO.UserRoles.DistrictHead)).FirstOrDefault();
                            if (distUser != null)
                            {
                                result = 3;//return DH exists
                            }
                            else
                            {
                                userDTO = FillUserDTO(userDTO);
                                personalDTO = FillPersonalDTO(personalDTO);
                                companyDTO = FillCompanyDTO(companyDTO);
                                partnerDTO = FillPartnerDTO(partnerDTO);
                                bankDTO = FillBankDTO(bankDTO);
                                accountDTO = FillAccountDTO(accountDTO);
                                result = InsertUserDetails(userDTO, personalDTO, companyDTO, partnerDTO, bankDTO, accountDTO);
                            }
                        }
                        else
                        {
                            userDTO = FillUserDTO(userDTO);
                            personalDTO = FillPersonalDTO(personalDTO);
                            companyDTO = FillCompanyDTO(companyDTO);
                            partnerDTO = FillPartnerDTO(partnerDTO);
                            bankDTO = FillBankDTO(bankDTO);
                            accountDTO = FillAccountDTO(accountDTO);
                            result = InsertUserDetails(userDTO, personalDTO, companyDTO, partnerDTO, bankDTO, accountDTO);
                        }
                    }
                    else
                    {
                        result = 1; // user Exists
                    }
                }
                return result;
            }
            catch (Exception)
            {                
                throw;
            }
        }

        public List<UserDTO> GetAllUsers()
        {
            try
            {
                UserRepository uRep = new UserRepository();
                RoleService roleService = new RoleService();
                List<UserDTO> lstUsers = uRep.GetAllUsers().ToList();
                List<RoleDTO> lstRoles = roleService.GetUserRoles();
                lstUsers = lstUsers.Join(lstRoles, u => u.RoleId, r => r.RoleId, (u, r) => new {u, r})
                    .Select(nu => new UserDTO()
                    {
                        UserId = nu.u.UserId,
                        Name = nu.u.Name,
                        Status = nu.u.Status,
                        RoleId = nu.u.RoleId
                    }).ToList();
                return lstUsers;
            }
            catch (Exception)
            {                
                throw;
            }            
        }
        #endregion

        #region "Private Methods"
        private int InsertUserDetails(UserDTO userDTO, PersonalDTO personalDTO, CompanyDTO companyDTO, PartnerDTO partnerDTO, BankDTO bankDTO, AccountDTO accountDTO)
        {
            UserRepository userRep = new UserRepository(); 
            PersonalRepository pRep = new PersonalRepository();
            CompanyRepository cRep = new CompanyRepository();
            PartnerRepository partnerRep = new PartnerRepository();
            BankRepository bankRep = new BankRepository();
            AccountRepository accountRep = new AccountRepository();
            int result = 0;

            var option = new TransactionOptions();
            option.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            using (var scope = new TransactionScope(TransactionScopeOption.Required, option))
            {
                int userId = userRep.CreateUser(userDTO);
                //var user = userRep.GetByUserName(userDTO.Name);
                if (userId > 0)
                {
                    personalDTO.UserId = userId;
                    int personalId = pRep.Create(personalDTO);
                    if (personalId > 0)
                    {
                        companyDTO.UserId = userId;
                        int cId = cRep.Create(companyDTO);
                        if (cId > 0)
                        {
                            partnerDTO.UserId = userId;
                            partnerDTO.CompanyId = cId;
                            int pId = partnerRep.Create(partnerDTO);
                            if (pId > 0)
                            {
                                int bankId = bankRep.Create(bankDTO);
                                if (bankId > 0)
                                {
                                    accountDTO.BankDetailsId = bankId;
                                    accountDTO.UserId = userId;
                                    int accId = accountRep.Create(accountDTO);
                                    if (accId > 0)
                                    {
                                        result = -1;
                                        scope.Complete();
                                    }
                                    else
                                    {
                                        result = 4;//Account not created
                                    }
                                }
                                else
                                {
                                    result = 5;//Bank details not created
                                }
                            }
                            else
                            {
                                result = 6;//Partner Details not created
                            }
                        }
                        else
                        {
                            result = 7;//Company deatils not created
                        }
                    }
                    else
                    {
                        result = 8;//Personal details not created
                    }
                }
                else
                {
                    result = 9;//User details not created
                }
            }
            return result;
        }

        private UserDTO FillUserDTO(UserDTO userDTO)
        {
            userDTO.CRDTM = DateTime.UtcNow;
            userDTO.UPDTM = DateTime.UtcNow;
            userDTO.IsActive = true;
            userDTO.IsDelete = false;
            return userDTO;
        }

        private PersonalDTO FillPersonalDTO(PersonalDTO personalDTO)
        {
            personalDTO.CRDTM = DateTime.UtcNow;
            personalDTO.UPDTM = DateTime.UtcNow;
            personalDTO.IsActive = true;
            personalDTO.IsDelete = false;
            return personalDTO;
        }

        private CompanyDTO FillCompanyDTO(CompanyDTO companyDTO)
        {
            companyDTO.CRDTM = DateTime.UtcNow;
            companyDTO.UPDTM = DateTime.UtcNow;
            companyDTO.IsActive = true;
            companyDTO.IsDelete = false;
            return companyDTO;
        }

        private PartnerDTO FillPartnerDTO(PartnerDTO partnerDTO)
        {
            partnerDTO.CRDTM = DateTime.UtcNow;
            partnerDTO.UPDTM = DateTime.UtcNow;
            partnerDTO.IsActive = true;
            partnerDTO.IsDelete = false;
            return partnerDTO;
        }

        private BankDTO FillBankDTO(BankDTO bankDTO)
        {
            bankDTO.CRDTM = DateTime.UtcNow;
            bankDTO.UPDTM = DateTime.UtcNow;
            bankDTO.IsActive = true;
            bankDTO.IsDelete = false;
            return bankDTO;
        }

        private AccountDTO FillAccountDTO(AccountDTO accountDTO)
        {
            accountDTO.CRDTM = DateTime.UtcNow;
            accountDTO.UPDTM = DateTime.UtcNow;
            accountDTO.IsActive = true;
            accountDTO.IsDelete = false;
            return accountDTO;
        }
        #endregion
    }
}
