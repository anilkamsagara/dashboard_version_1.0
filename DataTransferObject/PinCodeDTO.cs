﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObject
{
    public class PinCodeDTO
    {
        public int PinCodeId { get; set; }
        public string PinCode { get; set; }
        public string PinCodeLocation { get; set; }
        public int RTOId { get; set; }
        public System.DateTime CRDTM { get; set; }
        public System.DateTime UPDTM { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }

        public virtual RtoDTO RtoDTO { get; set; }
    }
}
