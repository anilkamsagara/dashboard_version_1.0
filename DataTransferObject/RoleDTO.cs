﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObject
{
    public class RoleDTO
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public System.DateTime CRDTM { get; set; }
        public System.DateTime UPDTM { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public bool IsAdmin { get; set; }

        public enum UserRoles
        {
            SuperAdmin = 1,
            StateHead = 2,
            DistrictHead = 3,
            BusinessAssociate = 4
        }
    }
}
