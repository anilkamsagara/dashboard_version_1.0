﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObject
{
    public class DriverDTO
    {
        public int DriverId { get; set; }
        public string DriverName { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string DLNumber { get; set; }
        public byte[] DriverPhoto { get; set; }
        public byte[] FC_Photo { get; set; }
        public long Mobile { get; set; }
        public string Email { get; set; }
        public string PanCard { get; set; }
        public string AadharCard { get; set; }
        public System.DateTime DLValidity { get; set; }

        public System.DateTime CRDTM { get; set; }
        public System.DateTime UPDTM { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
    }
}
