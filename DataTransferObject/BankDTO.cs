﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObject
{
    public class BankDTO
    {
        public int BankDetailsId { get; set; }
        public string Address { get; set; }
        public string MICRCode { get; set; }
        public string IFSCCode { get; set; }
        public System.DateTime CRDTM { get; set; }
        public System.DateTime UPDTM { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public int BankId { get; set; }
        public virtual BanksInIndiaDTO BanksInIndia { get; set; }
    }
}
