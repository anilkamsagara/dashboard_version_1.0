﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObject
{
    public class RtoDTO
    {
        public int RTOId { get; set; }
        public string RTOCode { get; set; }
        public string RTOLocation { get; set; }
        public int DistrictId { get; set; }
        public System.DateTime CRDTM { get; set; }
        public System.DateTime UPDTM { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }

        public virtual DistrictDTO DistrictDTO { get; set; }
    }
}
