﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObject
{
    public class VehicleOwnerDTO
    {
        public int VehicleOwnerId { get; set; }
        public string VehicleOwnerName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public int Pincode { get; set; }
        public string District { get; set; }
        public int StateId { get; set; }
        public int CountryId { get; set; }
        public int Age { get; set; }
        public long Mobile { get; set; }
        public string Email { get; set; }
        public string PAN { get; set; }
        public string AadharCard { get; set; }
        public System.DateTime CRDTM { get; set; }
        public System.DateTime UPDTM { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }

        public virtual CountryDTO CountryDTO { get; set; }
        public virtual StateDTO StateDTO { get; set; }
    }
}
