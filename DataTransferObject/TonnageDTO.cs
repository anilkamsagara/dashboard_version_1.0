﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObject
{
    public class TonnageDTO
    {
        public int TonnageId { get; set; }
        public string TonnageFrom { get; set; }
        public string TonnageTo { get; set; }
        public double TonnageCost { get; set; }
        public string TonnageRange { get; set; }
        public System.DateTime CRDTM { get; set; }
        public System.DateTime UPDTM { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
    }
}
