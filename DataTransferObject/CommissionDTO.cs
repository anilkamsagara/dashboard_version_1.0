﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObject
{
    public class CommissionDTO
    {
        public int CommissionId { get; set; }
        public string CommissionBA { get; set; }
        public string CommissionDH { get; set; }
        public string CommissionSH { get; set; }
        public string RenewalCost { get; set; }
        public int UserId { get; set; }
        public System.DateTime CRDTM { get; set; }
        public System.DateTime UPDTM { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }

        public virtual UserDTO UserDTO { get; set; }
    }
}
