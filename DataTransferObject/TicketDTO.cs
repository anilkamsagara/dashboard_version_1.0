﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObject
{
    public class TicketDTO
    {
        public int TicketId { get; set; }
        public string BAPermission { get; set; }
        public string BAComments { get; set; }
        public string DHPermission { get; set; }
        public string DHComments { get; set; }
        public string SHPermission { get; set; }
        public string SHComments { get; set; }
        public string SAPermission { get; set; }
        public string SAComments { get; set; }
        public int GoodsTypeId { get; set; }
        public Nullable<System.DateTime> TicketStartDate { get; set; }
        public Nullable<System.DateTime> TicketEndDate { get; set; }
        public Nullable<int> RenewalCounter { get; set; }
        public int TonnageId { get; set; }
        public int UserId { get; set; }
        public int VehicleId { get; set; }
        public int DriverId { get; set; }
        public System.DateTime CRDTM { get; set; }
        public System.DateTime UPDTM { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public int TicketStatusId { get; set; }
        public double Amount { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string District { get; set; }
        public int PinCode { get; set; }
        public string Address { get; set; }
        public string City { get; set; }

        public virtual DriverDTO DriverDTO { get; set; }
        public virtual TonnageDTO TonnageDTO { get; set; }
        public virtual UserDTO UserDTO { get; set; }
        public virtual VehicleDTO VehicleDTO { get; set; }
        public virtual TicketStatusDTO TicketStatusDTO { get; set; }
        public virtual GoodsTypeDTO GoodsTypeDTO { get; set; }
    }
}
