﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObject
{
    public class DocumentDTO
    {
        public int DocumentId { get; set; }
        public string DocumentName { get; set; }
        public string PanCardProof { get; set; }
        public string AadharCardProof { get; set; }
        public string CompanyRegistration { get; set; }
        public string CompanyAddressProof { get; set; }
        public string PartnerAddressProof { get; set; }
        public string MobileBill { get; set; }
        public string OfficeRentalDocument { get; set; }
        public int UserId { get; set; }
        public System.DateTime CRDTM { get; set; }
        public System.DateTime UPDTM { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }

        public virtual UserDTO UserDTO { get; set; }
    }
}
