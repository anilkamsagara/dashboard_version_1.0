﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObject
{
    public class PartnerDTO
    {
        public int PartnerId { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public int UserId { get; set; }
        public int CompanyId { get; set; }
        public System.DateTime CRDTM { get; set; }
        public System.DateTime UPDTM { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }

        public virtual CompanyDTO CompanyDTO { get; set; }
        public virtual UserDTO UserDTO { get; set; }
    }
}
