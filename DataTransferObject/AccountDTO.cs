﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObject
{
    public class AccountDTO
    {
        public int AccountId { get; set; }
        public double Funds { get; set; }
        public int BankDetailsId { get; set; }
        public int UserId { get; set; }
        public System.DateTime CRDTM { get; set; }
        public System.DateTime UPDTM { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public Int64 AccountNo { get; set; }

        public virtual BankDTO BankDTO { get; set; }
        public virtual UserDTO UserDTO { get; set; }
    }
}
