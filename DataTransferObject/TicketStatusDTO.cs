﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObject
{
    public class TicketStatusDTO
    {
        public int TicketStatusId { get; set; }
        public string TicketStatusDesc { get; set; }

        public enum TicketStatus
        {
            SA_Approved = 1,
            SA_Rejected = 2,
            SH_Approved = 3,
            SH_Rejected = 4,
            DH_Approved = 5,
            DH_Rejected = 6,
            BA_Approved = 7,
            BA_Rejected = 8,
            SA_Pending = 9,
            SH_Pending = 10, 
            DH_Pending = 11,
            BA_Pending = 12
        }
    }
}
