﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObject
{
    public class PersonalDTO
    {
        public int PersonalId { get; set; }
        //public string Name { get; set; }
        public string Address { get; set; }
        public int Age { get; set; }
        public string EmailId { get; set; }
        public long MobileNumber { get; set; }
        public string PanCard { get; set; }
        public string AadharCardNumber { get; set; }
        //public string EmployeeType { get; set; }
        public int UserId { get; set; }
        public System.DateTime CRDTM { get; set; }
        public System.DateTime UPDTM { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }
        public string City { get; set; }

        public virtual UserDTO UserDTO { get; set; }
    }
}
