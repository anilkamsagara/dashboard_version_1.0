﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObject
{
    public class VehicleLocationDTO
    {
        public int VehicleLocationId { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public int DistrictId { get; set; }
        public int StateId { get; set; }
        public int CountryId { get; set; }
        public string Location { get; set; }
        public int VehicleId { get; set; }
        public System.DateTime CRDTM { get; set; }
        public System.DateTime UPDTM { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }

        public virtual CountryDTO CountryDTO { get; set; }
        public virtual DistrictDTO DistrictDTO { get; set; }
        public virtual StateDTO StateDTO { get; set; }
        public virtual VehicleDTO VehicleDTO { get; set; }
    }
}
