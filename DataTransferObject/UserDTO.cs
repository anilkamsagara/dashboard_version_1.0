﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObject
{
    public class UserDTO
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Status { get; set; }
        public int RoleId { get; set; }
        public int? PinCodeId { get; set; }
        public int? RTOId { get; set; }
        public int? DistrictId { get; set; }
        public int StateId { get; set; }
        public int CountryId { get; set; }
        public System.DateTime CRDTM { get; set; }
        public System.DateTime UPDTM { get; set; }
        public bool IsActive { get; set; }
        public bool IsDelete { get; set; }

        //public string Name { get; set; } = "Default Name"
    }
}
