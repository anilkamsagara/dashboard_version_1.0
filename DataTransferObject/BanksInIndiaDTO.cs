﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObject
{
    public class BanksInIndiaDTO
    {
        public int BankId { get; set; }
        public string BankName { get; set; }
    }
}
