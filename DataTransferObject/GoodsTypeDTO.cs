﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferObject
{
    public class GoodsTypeDTO
    {
        public int GoodsTypeId { get; set; }
        public string GoodsTypeName { get; set; }
    }
}
